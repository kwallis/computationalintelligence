\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {2}{Einleitung}{4}{0}{1}
\beamer@subsectionintoc {2}{1}{Kontext}{5}{0}{1}
\beamer@subsectionintoc {2}{2}{Grundlegende Idee}{6}{0}{1}
\beamer@subsectionintoc {2}{3}{Geschichte}{10}{0}{1}
\beamer@sectionintoc {3}{Detail}{11}{0}{2}
\beamer@subsectionintoc {3}{1}{Agent und Umfeld}{12}{0}{2}
\beamer@subsectionintoc {3}{2}{State}{14}{0}{2}
\beamer@subsectionintoc {3}{3}{Policy}{15}{0}{2}
\beamer@subsectionintoc {3}{4}{Markov Process}{16}{0}{2}
\beamer@subsectionintoc {3}{5}{Markov Reward Process}{17}{0}{2}
\beamer@subsectionintoc {3}{6}{Markov Decision Process}{19}{0}{2}
\beamer@subsectionintoc {3}{7}{Value-Funktionen}{21}{0}{2}
\beamer@sectionintoc {4}{Problemstellung}{23}{0}{3}
\beamer@sectionintoc {5}{Grundlegende Methoden}{29}{0}{4}
\beamer@subsectionintoc {5}{1}{Dynamic Programming}{30}{0}{4}
\beamer@subsubsectionintoc {5}{1}{1}{Einf\IeC {\"u}hrung}{30}{0}{4}
\beamer@subsubsectionintoc {5}{1}{2}{...}{31}{0}{4}
\beamer@subsubsectionintoc {5}{1}{3}{Value Iteration Algorithmus}{32}{0}{4}
\beamer@subsubsectionintoc {5}{1}{4}{Beispiel: Spieler-Problem}{33}{0}{4}
\beamer@subsubsectionintoc {5}{1}{5}{Eigenschaften}{35}{0}{4}
\beamer@subsectionintoc {5}{2}{Monte Carlo}{36}{0}{4}
\beamer@subsubsectionintoc {5}{2}{1}{Einf\IeC {\"u}hrung}{36}{0}{4}
\beamer@subsubsectionintoc {5}{2}{2}{Backup}{37}{0}{4}
\beamer@subsubsectionintoc {5}{2}{3}{Action-Value Funktion}{41}{0}{4}
\beamer@subsubsectionintoc {5}{2}{4}{Eigenschaften}{47}{0}{4}
\beamer@subsectionintoc {5}{3}{Temporal Difference}{48}{0}{4}
\beamer@sectionintoc {6}{Erweiterte Methoden}{52}{0}{5}
\beamer@subsectionintoc {6}{1}{SARSA-Learning}{53}{0}{5}
\beamer@subsectionintoc {6}{2}{Q-Learning}{55}{0}{5}
\beamer@sectionintoc {7}{Mountain Car Problem}{58}{0}{6}
\beamer@subsectionintoc {7}{1}{Einf\IeC {\"u}hrung/Problemstellung}{59}{0}{6}
\beamer@subsectionintoc {7}{2}{Formale Beschreibung}{60}{0}{6}
\beamer@subsectionintoc {7}{3}{Modellierung des MDP}{61}{0}{6}
\beamer@subsectionintoc {7}{4}{Umsetzungen}{62}{0}{6}
\beamer@sectionintoc {8}{Ausblick}{63}{0}{7}
\beamer@sectionintoc {9}{Zusammenfassung}{65}{0}{8}
