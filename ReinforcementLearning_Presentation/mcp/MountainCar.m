function MountainCar(startPos, startVelocity, max_time, enginePower, ascendingPower, brain)
	
    startVelocity = 0;
    max_time = 3000;
    enginePower = 0.15;
    ascendingPower = 2;
    brain = LoadBrain('brain');
	
    stillInValley = -1;
	flyingRight = -2;
	flyingLeft = -3;
	notFinished = 0;
	success = 1;
    [mountain, mountainAscending] = LoadMountain();
    startPos = GetStartPos(mountain, mountainAscending);
    targetPos = size(mountain, 2) - 5;
	carLength = 10;
	carHeight = 0.05;
	
	figure(3);
	%mountain = mountain .* 300;
	ErrorCode = notFinished;
	currPos = startPos;
	currVelocity = startVelocity;
	engineSetting = 0;
    time = 0;
    epsilon = 0;
	while ErrorCode == notFinished
		clf;
		area(mountain, 'FaceColor', 'g');
		hold on;
		%plot([0, 2], 'w');
        Coin(targetPos, mountain(targetPos));
		DrawCar(carLength, carHeight, currPos, mountain(currPos), mountainAscending(currPos), carHeight/2, carLength/2);
		axis([1, size(mountain, 2), 0, 1.2]);
        title(strcat('Velocity: ', num2str(currVelocity)));
        drawnow;
		p = GetPageForVelocity(currVelocity);
		policy = brain(:, ceil(currPos/20), p);
		[engineSetting, policyIndex, greedy] = GetEngineSetting(policy, epsilon);
        engineSetting
		[currPos, currVelocity, ErrorCode] = Drive(currPos, targetPos, startPos, currVelocity, engineSetting, mountain, mountainAscending, enginePower, ascendingPower, time, max_time);
        if(time > 1)
            epsilon = 0;
        end;
        time = time + 1;
    end %while
	hold off;
    ErrorCode
    time
%	for i=1:size(mountain, 2)
%		clf;
%		plot(mountain);
%		hold on;
%		%plot([0, 2], 'w');
%		%carPos = size(plateau, 2) + size(leftAscentAscending, 2) + size(valley, 2)/2;
%		carPos = i;
%		DrawCar(carLength, carHeight, carPos, mountain(carPos), mountainAscending(carPos), carHeight/2, carLength/5);
%		drawnow;
%	end %for
%	hold off;
end %function

function DrawCar(length, height, start, ground, ascending, tireRadiusY, tireRadiusX)
	bottom = (start:start+length) * ascending;
	if bottom(1) < ground
		lift = abs(ground - bottom(1));
	elseif ground < bottom(1)
		lift = -abs(bottom(1) - ground);
	else 
		lift = 0;
	end %if
	bottom = bottom + lift + tireRadiusY;
	top = bottom + height;
	left = [bottom(1), top(1)];
	right = [bottom(size(bottom, 2)), top(size(top, 2))];
	hold on;
%	plot(start:(start+length), top);
%	plot(start:(start+length), bottom);
%	plot([start, start+0.001], left);
%	plot([start+length, start+length+0.001], right);
%	DrawCircle(tireRadiusX, tireRadiusY, start, bottom(1));
%	DrawCircle(tireRadiusX, tireRadiusY, start+length, bottom(size(bottom,2)));
	DrawCircle(tireRadiusX, tireRadiusY, start, bottom(1));
end %function

function DrawCircle(rX, rY, x, y)
	t = linspace(0,2*pi,100)';
	circsx = rX.*cos(t) + x;
	circsy = rY.*sin(t) + y;
	plot(circsx, circsy);
end %function

function Coin(x, y)
    rX = 5;
    rY = 0.02;
    y = y + rY;
	t = linspace(0,2*pi,100)';
	circsx = rX.*cos(t) + x;
	circsy = rY.*sin(t) + y;
	area(circsx, circsy, 'FaceColor', 'y');
end %function

function pos = GetStartPos(mountain, mountainAscending)
	pos = 1;
	while mountainAscending(pos) >= 0
		pos = pos + 1;
	end %while
	while mountain(pos) > mountain(pos+1)
		pos = pos + 1;
	end %
end %function

