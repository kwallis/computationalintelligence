function MountainCarLearn
	[mountain, mountainAscending] = LoadMountain();
	max_time = inf;
	max_Epoch = 2000;
	enginePower = 0.15;
	ascendingPower = 2;
	startPos = GetStartPos(mountain, mountainAscending);
	targetPos = size(mountain, 2) - 5;
	measurementSpan = 10;
	epsilon = 0.4;
    %epsilon = 1;
	%epsilon = 0;
    %alpha = 0.5;
    alpha = 1;
	%gamma = 0.9995; %for max_Epoch = 10000
    %gamma = 0.9987; %for max_Epoch = 5000
    %gamma = 0.997; %for max_Epoch = 1000
    gamma = 1;
    %gamma = 0.5;
    
    epsilonDecay = 0.9987;
    
    avgTime = zeros(10, max_Epoch/measurementSpan);
    for i=1:10
        brain = LoadBrain('brainInit');
        %[brain, nFailed, nValley, nGoFlyingRight, nGoFlyingLeft, nSuccess, timeNeeded, timeMeasurement] = QLearning(startPos, targetPos, mountain, mountainAscending, enginePower, ascendingPower, brain, epsilon, alpha, gamma, epsilonDecay, max_time, measurementSpan, max_Epoch);
        [brain, nFailed, nValley, nGoFlyingRight, nGoFlyingLeft, nSuccess, timeNeeded, timeMeasurement] = SARSA(startPos, targetPos, mountain, mountainAscending, enginePower, ascendingPower, brain, epsilon, alpha, gamma, max_time, measurementSpan, max_Epoch);
        %[brain, nFailed, nValley, nGoFlyingRight, nGoFlyingLeft, nSuccess, timeNeeded, timeMeasurement] = MonteCarlo(startPos, targetPos, mountain, mountainAscending, enginePower, ascendingPower, brain, epsilon, alpha, gamma, max_time, measurementSpan, max_Epoch);
        avgTime(i,:) = timeMeasurement;
    end %for
    avgTime = mean(avgTime);
    avgTime
    %save('QLearning.txt', avgTime);
    dlmwrite('SARSALearning.txt', avgTime);
    nValley
	nGoFlyingLeft
	nGoFlyingRight
    nSuccess
	figure(1);
	clf;
	plot(avgTime);
    %hold on;
	%plot(1:measurementSpan:(measurementSpan*size(nFailed, 2)), nFailed, 'r');
	%plot(1:measurementSpan:(measurementSpan*size(nFailed, 2)), nValley, 'b');
	%plot(1:measurementSpan:(measurementSpan*size(nFailed, 2)), nGoFlyingLeft, 'y');
	%plot(1:measurementSpan:(measurementSpan*size(nFailed, 2)), nGoFlyingRight, 'k');
	%plot(1:measurementSpan:(measurementSpan*size(nFailed, 2)), nSuccess, 'g');
	%hold off;
	figure(2);
	clf;
	semilogy(timeNeeded);
	SaveBrain(brain, 'brain');
end %function

function pos = GetStartPos(mountain, mountainAscending)
	pos = 1;
	while mountainAscending(pos) >= 0
		pos = pos + 1;
	end %while
	while mountain(pos) > mountain(pos+1)
		pos = pos + 1;
	end %
end %function