function CreateMountain
	x = fliplr(0.1:0.1:6);
	x = x * -1;
	x = [x, 0:0.1:6];
	rightAscent = [];
	rightAscentAscending = [];
	for i=1:size(x,2)
		[y, y_stroke] = Ascent(x(i), 1, 1);
		rightAscent = [rightAscent, y];
		rightAscentAscending = [rightAscentAscending, y_stroke];
	end %for
	leftAscent = [];
	leftAscentAscending = [];
	for i=1:size(x,2)
		%[y, y_stroke] = Ascent(x(i), -1.33334, 0.75);
		[y, y_stroke] = Ascent(x(i), -1, 1);
		leftAscent = [leftAscent, y];
		leftAscentAscending = [leftAscentAscending, y_stroke];
	end %for
	valley = zeros(1, 25);
	plateau = ones(1, 100);
	mountain = [(plateau .* leftAscent(1)), leftAscent, valley, rightAscent, (plateau .* rightAscent(size(rightAscent, 2)))];
	mountainAscending = [(plateau - 1), leftAscentAscending, valley, rightAscentAscending, (plateau - 1)];
	dlmwrite('mountain.txt', mountain);
	dlmwrite('mountainAscending.txt', mountainAscending);
end %function

function [y, y_stroke] = Ascent(x, lamda, alpha)
	y = alpha/(1+exp(-lamda*x));
	y_stroke = (alpha*lamda*exp(lamda*x))/((exp(lamda*x)+1)^2);
end %function