function brain = GetInitialBrain(initialValue, mountain)
	for i=1:12
		brain(:,:,i) = ones(3, ceil(size(mountain, 2)/20)) * initialValue;
	end %for
end %function