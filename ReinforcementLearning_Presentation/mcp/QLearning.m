function [brain, nFailed, nValley, nGoFlyingRight, nGoFlyingLeft, nSuccess, timeNeeded, timeMeasurement] = QLearning(startPos, targetPos, mountain, mountainAscending, enginePower, ascendingPower, brain, epsilon, alpha, gamma, epsilonDecay, max_time, measurementSpan, max_Epoch)
	stillInValley = -1;
	flyingRight = -2;
	flyingLeft = -3;
	notFinished = 0;
	success = 1;
	policyBrain = brain;
	bestBrain = brain;
	bestTime = inf;
	
	%brain(:,targetPos,:) = max_time;
	brain(:,ceil(targetPos/20),:) = 10;
	
	counter = 0;
    timeMeasurement = [];
	timeNeeded = [];
	nFailed = [];
	nValley = [];
	nGoFlyingLeft = [];
	nGoFlyingRight = [];
	nSuccess = [];
	ec = notFinished;
	sIV = 0;
	fR = 0;
	fL = 0;
	succ = 0;
	for i=1:max_Epoch
		i
		%currPos = floor(rand() * 464 + 1);
		currPos = startPos;
		currVelocity = 0;
		ec = notFinished;
		time = 0; %is also reward
		prevPos = -1;
		prevPI = -1;
		prevPage = -1;
		p = GetPageForVelocity(currVelocity);
		returns = [];
		while (ec == notFinished)
			policy = policyBrain(:, ceil(currPos/20), p);
            [engineSetting, policyIndex, greedy] = GetEngineSetting(policy, epsilon);
			prevPage = p;
			prevPI = policyIndex;
			prevPos = currPos;
			[currPos, currVelocity, ec] = Drive(currPos, targetPos, startPos, currVelocity, engineSetting, mountain, mountainAscending, enginePower, ascendingPower, time, max_time);
			p = GetPageForVelocity(currVelocity);
			%backup
			returns = [returns, [prevPI; prevPos; prevPage]];
			brain(prevPI, ceil(prevPos/20), prevPage) = brain(prevPI, ceil(prevPos/20), prevPage) + alpha * (-1 + gamma*max(brain(:,ceil(currPos/20),p)) - brain(prevPI,ceil(prevPos/20),prevPage));
			time = time + 1;
            policyBrain = brain;
		end %while
		%alpha = alpha * gamma;
        epsilon = epsilon * epsilonDecay;
		
		reward = -max_time;
		%reward = -1;
		ErrorCode = ec;
		if (ErrorCode == flyingLeft)
			%reward = reward - (max_time * 10000000);
            %reward = -10;
		elseif (ErrorCode == success || ErrorCode == flyingRight)
			%reward = reward + 10000000000;
            reward = max_time;
			%reward = 1;
		end %if
		%policy = brain(:, currPos, p);
        %[engineSetting, policyIndex, greedy] = GetEngineSetting(policy, epsilon);
		%brain(:, currPos, :) = reward;
        %currPos = ceil(currPos/50);
		%idx = size(returns, 2);
		%for i=1:(idx-1)
		%	prevPI = returns(1, idx - i);
		%	prevPos = ceil(returns(2, idx - i)/50);
		%	prevPage = returns(3, idx - i);
		%	brain(prevPI, prevPos, prevPage) = brain(prevPI, prevPos, prevPage) + alpha * (-1 + gamma*max(brain(:,currPos,p)) - brain(prevPI,prevPos,prevPage));
		%	currPos = ceil(returns(2, idx - i)/50);
		%	p = returns(3, idx - i);
		%end %for
        %policyBrain = brain;
		
		timeNeeded = [timeNeeded, time];
		if (ErrorCode == stillInValley)
			sIV = sIV + 1;
		elseif (ErrorCode == flyingLeft)
			fL = fL + 1;
		elseif (ErrorCode == flyingRight)
			fR = fR + 1;
		else
			succ = succ + 1;
		end %if
		counter = counter + 1;
        
		if (counter >= measurementSpan)
			nFailed = [nFailed, sIV + fL + fR];
			nValley = [nValley, sIV];
			nGoFlyingLeft = [nGoFlyingLeft, fL];
			nGoFlyingRight = [nGoFlyingRight, fR];
			nSuccess = [nSuccess, succ];
			counter = 0;
			sIV = 0;
			fR = 0;
			fL = 0;
            succ = 0;
            
            %% Test how well the solution performs
			currPos = startPos;
			currVelocity = 0;
			ErrorCode = notFinished;
			time = 0;
			while ErrorCode == notFinished
				p = GetPageForVelocity(currVelocity);
				policy = brain(:, ceil(currPos/20), p);
				[engineSetting, policyIndex, greedy] = GetEngineSetting(policy, 0);
				[currPos, currVelocity, ErrorCode] = Drive(currPos, targetPos, startPos, currVelocity, engineSetting, mountain, mountainAscending, enginePower, ascendingPower, time, max_time);
				if(time > 500)
					break;
				end;
				time = time + 1;
			end %while
			time
			if time < bestTime
				bestBrain = brain;
				bestTime = time;
			end %if
			timeMeasurement = [ timeMeasurement , bestTime ];
			% --------------------------
		end %if
	end %for
	brain = bestBrain;
end %function