function SaveBrain(brain, filename)
	for i=1:size(brain, 3)
		A = brain(:,:,i);
		dlmwrite(strcat(filename, '/brain', num2str(i), '.txt'), A);
	end %for
end %function