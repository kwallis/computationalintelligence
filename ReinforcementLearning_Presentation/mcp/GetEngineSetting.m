function [setting, index, greedy] = GetEngineSetting(policy, epsilon)
	[best, index] = max(policy);
	index = find(policy == best);
	index = index(floor(rand() * size(index, 1) + 1));
	greedy = 1;
	if rand() < epsilon
		index = floor(rand() * size(policy, 1) + 1);
		best = policy(index);
		greedy = 0;
	end %if
    setting = index - 2;
end %function