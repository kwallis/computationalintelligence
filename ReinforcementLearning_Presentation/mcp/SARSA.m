function [brain, nFailed, nValley, nGoFlyingRight, nGoFlyingLeft, nSuccess, timeNeeded, timeMeasurement] = SARSA(startPos, targetPos, mountain, mountainAscending, enginePower, ascendingPower, brain, epsilon, alpha, gamma, max_time, measurementSpan, max_Epoch)
	stillInValley = -1;
	flyingRight = -2;
	flyingLeft = -3;
	notFinished = 0;
	success = 1;

	decay = 0.9998

    bestBrain = brain;
    bestTime = inf;

	counter = 0;
	timeMeasurement = [];
	timeNeeded = [];
	nFailed = [];
	nValley = [];
	nGoFlyingLeft = [];
	nGoFlyingRight = [];
	nSuccess = [];
	ec = notFinished;
	sIV = 0;
	fR = 0;
	fL = 0;
	succ = 0;
	for i=1:max_Epoch
		i
        %currPos = floor(rand() * 464) + 1;
		currPos =  startPos;
		currVelocity = 0;
		ec = notFinished;
		time = 0; %is also reward
		prevPos = -1;
		prevPI = -1;
		prevPage = -1;
		p = GetPageForVelocity(currVelocity);
		policy = brain(:, ceil(currPos/20), p);
		[engineSetting, policyIndex, greedy] = GetEngineSetting(policy, epsilon);
		while (ec == notFinished)

			prevPage = p;
			prevPI = policyIndex;
			prevPos = currPos;
			[currPos, currVelocity, ec] = Drive(currPos, targetPos, startPos, currVelocity, engineSetting, mountain, mountainAscending, enginePower, ascendingPower, time, max_time);
			reward = -1;
			p = GetPageForVelocity(currVelocity);
			policy = brain(:, ceil(currPos/20), p);
			[engineSetting_next, policyIndex_next, greedy] = GetEngineSetting(policy, epsilon);
			
            if ec == success
                reward = 0;
                brain(prevPI, ceil(prevPos/20), prevPage) = brain(prevPI, ceil(prevPos/20), prevPage) + alpha * (reward + gamma*0 - brain(prevPI,ceil(prevPos/20),prevPage)); 
            else
                brain(prevPI, ceil(prevPos/20), prevPage) = brain(prevPI, ceil(prevPos/20), prevPage) + alpha * (reward + gamma*brain(policyIndex_next, ceil(currPos/20), p) - brain(prevPI,ceil(prevPos/20),prevPage));
            end %if
            time = time + 1;
			
			engineSetting = engineSetting_next;
			policyIndex = policyIndex_next;
			
		end %while
		%alpha = alpha * gamma;
        epsilon = epsilon * decay;
		
		timeNeeded = [timeNeeded, time];
		ErrorCode = ec;
		if (ErrorCode == stillInValley)
			sIV = sIV + 1;
		elseif (ErrorCode == flyingLeft)
			fL = fL + 1;
		elseif (ErrorCode == flyingRight)
			fR = fR + 1;
		else
			succ = succ + 1;
		end %if
		counter = counter + 1;
		if (counter >= measurementSpan)
			nFailed = [nFailed, sIV + fL + fR];
			nValley = [nValley, sIV];
			nGoFlyingLeft = [nGoFlyingLeft, fL];
			nGoFlyingRight = [nGoFlyingRight, fR];
			nSuccess = [nSuccess, succ];
			counter = 0;
			sIV = 0;
			fR = 0;
			fL = 0;
            succ = 0;
			
			%% Test how well the solution performs
			currPos = startPos;
			currVelocity = 0;
			ErrorCode = notFinished;
			time = 0;
			while ErrorCode == notFinished
				p = GetPageForVelocity(currVelocity);
				policy = brain(:, ceil(currPos/20), p);
				[engineSetting, policyIndex, greedy] = GetEngineSetting(policy, 0);
				[currPos, currVelocity, ErrorCode] = Drive(currPos, targetPos, startPos, currVelocity, engineSetting, mountain, mountainAscending, enginePower, ascendingPower, time, max_time);
				if(time > 500)
					break;
				end;
				time = time + 1;
			end %while
			time

            if time < bestTime
                bestBrain = brain;
                bestTime = time;
            end %if
			timeMeasurement = [ timeMeasurement , bestTime ];
			% --------------------------
		end %if
	end %for
    brain = bestBrain;
end %function