function [newPos, newVelocity, ErrorCode] = Drive(currPos, targetPos, startPos, currVelocity, engine, mountain, ascending, enginePower, ascendingPower, time, max_time)
	stillInValley = -1;
	flyingRight = -2;
	flyingLeft = -3;
	notFinished = 0;
	success = 1;
	newVelocity = currVelocity;
	newPos = currPos;
	for i=1:5
		newVelocity = newVelocity + engine * enginePower - ascending(newPos) * ascendingPower;
		if newVelocity > 0
			newPos = newPos + 1;
		elseif newVelocity < 0
			newPos = newPos - 1;
		else
			newPos = newPos;
		end %if
		if newPos < 1
			newPos = 1;
		elseif newPos > targetPos
			newPos = targetPos;
		end %if
	end %for
	
	if newPos <= 1
		%ErrorCode = flyingLeft;
		ErrorCode = notFinished;
		newVelocity = 0;
		newPos = startPos;
%	elseif newPos > size(ascending, 2)
%		ErrorCode = flyingRight;
	elseif time >= max_time
		ErrorCode = stillInValley;
	elseif currPos >= targetPos
		% && abs(newVelocity) <= 0.1
		ErrorCode = success;
		newPos = targetPos;
	else
		ErrorCode = notFinished;
	end %if
end %function