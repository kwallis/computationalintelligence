function [brain, nFailed, nValley, nGoFlyingRight, nGoFlyingLeft, nSuccess, timeNeeded, timeMeasurement] = MonteCarlo(startPos, targetPos, mountain, mountainAscending, enginePower, ascendingPower, brain, epsilon, alpha, gamma, max_time, measurementSpan, max_Epoch)
	stillInValley = -1;
	flyingRight = -2;
	flyingLeft = -3;
	notFinished = 0;
	success = 1;
	rewardCount = zeros(size(brain));
	bestBrain = brain;
	bestTime = inf;
	
	counter = 0;
	timeMeasurement = [];
	timeNeeded = [];
	nFailed = [];
	nValley = [];
	nGoFlyingLeft = [];
	nGoFlyingRight = [];
	nSuccess = [];
	ec = notFinished;
	sIV = 0;
	fR = 0;
	fL = 0;
	succ = 0;
	for i=1:max_Epoch
		i
		currPos = startPos;
		currVelocity = 0;
		ec = notFinished;
		time = 0; %is also reward
		prevPos = -1;
		prevPI = -1;
		prevPage = -1;
		p = GetPageForVelocity(currVelocity);
		returns = [];
		while (ec == notFinished)
			policy = brain(:, ceil(currPos/20), p);
            [engineSetting, policyIndex, greedy] = GetEngineSetting(policy, epsilon);
			prevPage = p;
			prevPI = policyIndex;
			prevPos = currPos;
			[currPos, currVelocity, ec] = Drive(currPos, targetPos, startPos, currVelocity, engineSetting, mountain, mountainAscending, enginePower, ascendingPower, time, max_time);
			p = GetPageForVelocity(currVelocity);
			%backup
			returns = [returns,[prevPage; ceil(prevPos/20); prevPI; time]];
			time = time + 1;
		end %while
		%alpha = alpha * gamma;
        epsilon = epsilon * gamma;
		
		reward = -max_time;
		%reward = 0;
		%reward = -1;
		ErrorCode = ec;
		if (ErrorCode == flyingLeft)
			%reward = -max_time;
			%reward = reward - (max_time * 10000000);
            %reward = -10;
			%reward = reward - max_time
		elseif (ErrorCode == success || ErrorCode == flyingRight)
			%reward = reward + 10000000000;
            %reward = max_time - time;
			reward = 0;
			%reward = 1;
		end %if
		
		for i=1:size(returns,2)
			brain(returns(3,i), returns(2,i), returns(1,i)) = ((brain(returns(3,i), returns(2,i), returns(1,i)) * rewardCount(returns(3,i), returns(2,i), returns(1,i))) + reward - time + returns(4,i))/(rewardCount(returns(3,i), returns(2,i), returns(1,i))+1);
			rewardCount(returns(3,i), returns(2,i), returns(1,i)) = rewardCount(returns(3,i), returns(2,i), returns(1,i)) + 1;
		end %for
		
		timeNeeded = [timeNeeded, time];
		if (ErrorCode == stillInValley)
			sIV = sIV + 1;
		elseif (ErrorCode == flyingLeft)
			fL = fL + 1;
		elseif (ErrorCode == flyingRight)
			fR = fR + 1;
		else
			succ = succ + 1;
		end %if
		counter = counter + 1;
		if (counter >= measurementSpan)
			nFailed = [nFailed, sIV + fL + fR];
			nValley = [nValley, sIV];
			nGoFlyingLeft = [nGoFlyingLeft, fL];
			nGoFlyingRight = [nGoFlyingRight, fR];
			nSuccess = [nSuccess, succ];
			counter = 0;
			sIV = 0;
			fR = 0;
			fL = 0;
            succ = 0;
			
			%% Test how well the solution performs
			currPos = startPos;
			currVelocity = 0;
			ErrorCode = notFinished;
			time = 0;
			while ErrorCode == notFinished
				p = GetPageForVelocity(currVelocity);
				policy = brain(:, ceil(currPos/20), p);
				[engineSetting, policyIndex, greedy] = GetEngineSetting(policy, 0);
				[currPos, currVelocity, ErrorCode] = Drive(currPos, targetPos, startPos, currVelocity, engineSetting, mountain, mountainAscending, enginePower, ascendingPower, time, max_time);
				if(time > 500)
					break;
				end;
				time = time + 1;
			end %while
			time
			if time < bestTime
				bestBrain = brain;
				bestTime = time;
			end %if
			timeMeasurement = [ timeMeasurement , bestTime ];
			% --------------------------
		end %if
	end %for
	brain = bestBrain;
end %function