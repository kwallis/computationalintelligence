function Stat
    mc = dlmread('MCLearning.txt');
    sarsa = dlmread('SARSALearning.txt');
    q = dlmread('QLearning.txt');
	
   
	figure(1);
	clf;
    hold on;
	plot(mc);
	plot(sarsa, 'g');
	plot(q, 'r');
    legend('Monte-Carlo', 'SARSA', 'Q');
    xlabel('episode');
    ylabel('time/steps');
    hold off;
end %function