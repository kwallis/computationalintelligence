function brain = LoadBrain(filename)
	for i=1:12
		A = dlmread(strcat(filename, '/brain', num2str(i), '.txt'));
		brain(:,:,i) = A;
	end %for
end %function