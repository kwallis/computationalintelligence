function [mountain, mountainAscending] = LoadMountain
	mountain = dlmread('mountain.txt');
	mountainAscending = dlmread('mountainAscending.txt');
	if size(mountain, 1) > size(mountain, 2)
		mountain = mountain';
	end %if
	if size(mountainAscending, 1) > size(mountainAscending, 2)
		mountainAscending = mountainAscending';
	end %if
end %function