function p = GetPageForVelocity(v)
	if v < -20
		p = 1;
	elseif v > 19
		p = 12;
	elseif v >= 0.01 && v <= 0.01
		p = 7;
	elseif v < 0
		p = floor(v / 4 + 5 + 2);
	else
		p = floor(v / 4 + 5 + 2) + 1;
	end %if

	%if v < 0
	%	p = 1;
	%elseif v > 0
	%	p = 3;
	%else
	%	p = 2;
	%end %if
end %function