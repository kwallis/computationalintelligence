% Authors: Marco Sohm, Kevin Wallis
graphics_toolkit gnuplot
more off;
randn("state", 7);

dims = [4; 8; 16; 32; 64];
funs = ['QuadraticSphereFitness'; 'Ellipsoid1Fitness'; 'Ellipsoid2Fitness'; 'RosenbrockFitness'];

sigmaP = 1;
my = 3;
rho = 3;
lambda = 12;
sigma_stop = 1e-5;
F_target = 1e-4;  
trials = 15;
max_trials = 30;

colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];

ERT_over_N = [];
Ps_over_N = [];
erts = [];
pss = [];

for i=1:size(funs, 1)
  
  fun = funs(i, :); 
  fun = deblank(fun);                                                                                                                             
  disp('fun')
  disp(fun)
  
  for j=1:size(dims, 1)
    N = dims(j);
    disp('dimension')
    disp(N)
    yP = 10 * ones(1, N);
    tau = 1 / sqrt(2 * N);

    ERT_over_N(j) = 0;    
       
    % function [ert, ps] = ERT(func, fun, yP, sigmaP, sigma_stop, N, my, rho, lambda, tau, F_target)  
    [ert, ps] = feval('ERT','CMA_ES_RankMy', trials, fun, yP, sigmaP, sigma_stop, N, my, rho, lambda, tau, F_target, max_trials); 
    ERT_over_N(j) = ert;
    Ps_over_N(j) = ps;
  endfor
  erts(i,:) = ERT_over_N;
  pss(i,:) = Ps_over_N;
endfor  
  
figOffset = 1;

figure(figOffset)
figOffset++;
clf reset;
hold on;
for i=1:size(funs, 1)
	loglog(dims, erts(i,:), '-', 'Color',  colors(i), 'linewidth', 2);
endfor
loglog((1:100), 1e1*(1:100), 'g');
loglog((1:100), 1e2*(1:100), 'g');
loglog((1:100), 1e3*(1:100), 'g');
%loglog((1:100), 1e4*(1:100), 'g');
loglog((1:100), 1e1*(1:100).^2, 'c');
loglog((1:100), 1e2*(1:100).^2, 'c');
%loglog((1:100), 1e3*(1:100).^2, 'c');
%loglog((1:100), 1e4*(1:100).^2, 'c');
xlabel('N', 'FontSize', 12);
ylabel('ERT', 'FontSize', 12 );
legend(funs);

title(['(3/3_I, 12)-CMA_ES_RankMy; ERT'], 'FontSize', 12); 

hold off;
%tmp = strcat('./figures/Task1_', fun, '.png');
%print(tmp, '-dpng');

figure(figOffset)
figOffset++;
clf reset;
hold on;
for i=1:size(funs, 1)
	plot(dims, pss(i,:), '-', 'Color',  colors(i), 'linewidth', 2);
endfor
xlabel('N', 'FontSize', 12);
ylabel('Ps', 'FontSize', 12 );
legend(funs);

title(['(3/3_I, 12)-CMA_ES_RankMy; Ps'], 'FontSize', 12);

hold off;
%tmp = strcat('./figures/Task1_Ps_', fun, '.png');
%print(tmp, '-dpng');