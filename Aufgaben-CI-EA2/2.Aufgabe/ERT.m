function [ert, ps] = ERT(func, trials, fun, yP, sigmaP, sigma_stop, N, my, rho, lambda, tau, F_target, max_trials)   
	
	max_Succ = trials;
	trials = 0;
	ert = 0;
    ps = 0;
    
    E_ru = 0;
    E_rs = 0;
    
    nr_Succ = 0;        % number of successfull trials
    nr_Unsucc = 0;      % number of successfull trials
    nr_FE_Succ = 0;     % number of function evalutions in successfull trials
    nr_FE_Unsucc = 0;   % number of function evalutions in unsuccessfull trials
    
	while nr_Succ < max_Succ && trials <= max_trials	% max_Succ = number of requested successfull trials | max_trials = safeguard
    %for k=1:trials
      [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter, success] = feval(func, fun, yP, sigmaP, sigma_stop, my, lambda, F_target);
      trials++;
      if(success) 
        nr_Succ = nr_Succ + 1;
        nr_FE_Succ = nr_FE_Succ + f_counter;
      else 
        nr_FE_Unsucc = nr_FE_Unsucc + f_counter; 
      endif
      
      msg = strcat(int2str(trials), '. trial done');
      disp(msg)
    %endfor
	endwhile
    
    nr_Unsucc = trials - nr_Succ;
    ps = nr_Succ / trials;                % 2.12 slide 52
    
    if(nr_Unsucc != 0)
      E_ru = nr_FE_Unsucc / nr_Unsucc;    % 2.13 slide 52
    endif
    
    if(nr_Succ == 0)
      disp('No successfull computation -> ERT cannot be calculated!')
      ert = 0;
    else
      E_rs = nr_FE_Succ / nr_Succ;        % 2.13 slide 52
      ert = (1 / ps - 1) * E_ru + E_rs;     % 2.11 slide 51
    endif
endfunction