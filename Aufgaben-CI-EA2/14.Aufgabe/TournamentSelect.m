% X: parents, each column is a parent
% p: probability for selection of each parent, each column for a parent
function [v, w] = TournamentSelect(X, p)
	my = size(p, 2);
	pMax = 0;
	pFirst = 0;
	for j=1:my
		if pMax < p(j)
			pMax = p(j);
			pFirst = j;
		end; %if
	end; %for
	pMax = 0;
	pSecond = 0;
	for j=1:my
		if pMax < p(j) && j != pFirst
			pMax = p(j);
			pSecond = j;
		end; %if
	end; %for
	v = X(:, pFirst);
	w = X(:, pSecond);
end; %function