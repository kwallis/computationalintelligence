% my: Populationsgröße
% l: Genomlänge
% pc: probability for crossover
% pm: probability for flipping a bit
function [g, fDyn] = cGA (my, l, pc, pm, N, fun, select, crossover, maxIter = 5000)
	%initialize parents
	X = rand(l, 1) < 0.5;
	for j = 1:(my-1)
		X = [X,rand(l, 1) < 0.5];
	end; %for
	g = 0;
	while(1==1 &&  g < maxIter)
		%determine phis
		y = gpMapping(X(:, j), N);
		kleinPhi = feval(fun, y);
		grossPhi = kleinPhi;
		for j = 2:my
			y = gpMapping(X(:, j), N);
			kleinPhi = [kleinPhi, feval(fun, y)];
			grossPhi += kleinPhi(j);
		end; %for
		%average fitness of generation
		if g == 0
			fDyn = grossPhi/my;
		else
			fDyn = [fDyn, grossPhi/my];
		end; %if
		%determine p
		p = ones(1, my);
		for j = 1:my
			p(j) = kleinPhi(j)/grossPhi;
		end; %for
		%select and crossover
		Xtilde = ones(l, my);
		for j=1:2:my
			[v, w] = feval(select, X, p);
			if rand() < pc
				[Xtilde(:, j), Xtilde(:, j+1)] = feval(crossover, v, w);
			else
				Xtilde(:, j) = v;
				Xtilde(:, j+1) = w;
			end; %if
		end; %for
		%mutate
		for j=1:my
			mutate = rand(l, 1) < pm;
			X(:,j) = bitxor(Xtilde(:,j), mutate);
		end; %for
		g++;
	end; %while
	y = gpMapping(X(:, j), N);
	kleinPhi = feval(fun, y);
	grossPhi = kleinPhi;
	for j = 2:my
		y = gpMapping(X(:, j), N);
		kleinPhi = [kleinPhi, feval(fun, y)];
		grossPhi += kleinPhi(j);
	end; %for
	fDyn = [fDyn, grossPhi/my];
end; %function

function y = gpMapping(x, N)
	l = size(x, 1);
	n = floor(l/N);
	counter = 1;
	y = zeros(N, 1);
	for j=1:N
		if counter+n-1 > l
			y(j) = BinaryToInteger(x(counter:l));
		else
			y(j) = BinaryToInteger(x(counter:counter+n-1));
		end; %if
		counter += n;
	end;
end; %function

function y = BinaryToInteger(x)
	l = size(x, 1);
	high = (0:(l-1))';
	high = (ones(l, 1)*2).^high;
	y = x'*high;
end; %function