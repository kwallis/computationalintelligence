my = 80;
N = 15;
l = N*8;
pc = 0.6;
pm1 = 0.01;
pm2 = 0.001;
fun = "GE";
select = "TournamentSelect";
%select = "PropSelect";
crossover = "OnePointCrossover";

[g, fDyn] = cGA(my, l, pc, pm1, N, fun, select, crossover, 100);

g
fDyn

figure(2);
plot(fDyn);