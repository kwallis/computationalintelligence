% X: parents, each column is a parent
% p: probability for selection of each parent, each column for a parent
function [v, w] = PropSelect(X, p)
	my = size(p, 2);
	pSelect = rand();
	pCum = 0;
	for j=1:my
		pCum += p(j);
		if pSelect < pCum || (pSelect == 1 && pSelect == pCum)
			v = X(:, j);
			break;
		end; %if
	end; %for
	pSelect = rand();
	pCum = 0;
	for j=1:my
		pCum += p(j);
		if pSelect < pCum || (pSelect == 1 && pSelect == pCum)
			w = X(:, j);
			break;
		end; %if
	end; %for
end; %function