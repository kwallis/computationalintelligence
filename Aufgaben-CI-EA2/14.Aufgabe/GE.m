%15*8
function fitness = GE (x)
	max_iter = 20;
	stuetzstellen = -1:0.105:1;
	zielwerte = zeros(1,20);
	for i=1:20
		zielwerte(i) = TargetFunction(stuetzstellen(i));
	end %for
	fun = ["<expr>"];
	l = size(fun,1);
	noOfNonTerminals = 1;
	xIndex = 1;
	xLength = size(x,1);
	x
	iter = 0;
	while(noOfNonTerminals > 0 && iter <= max_iter)
		iter
		i = 1;
		while(i<=l)
		i
			if(isTerminal(strtrim(fun(i,:))) == 0)
				derivate = DerivateRule(strtrim(fun(i,:)), x(xIndex));
				noOfNonTerminals += CountNonTerminals(derivate) - 1;
				xIndex++;
				if(xIndex > xLength)
					xIndex = 1;
				end %if
				if(i == 1)
					start = "";
				else 
					start = fun(1:i-1,:);
				end %if
				if(i >= l)
					ending = "";
				else 
					ending = fun(i+1:l,:);
				end %if
				i = size([start; derivate],1);
				fun = [start; derivate; ending];
				l = size(fun,1);
			end %if
			i++;
		end %while
		iter++;
	end %while
	
	if(noOfNonTerminals > 0)
		fun = Terminalize(fun);
	end %if
	
	save(fun);
	
	fitness = 0;
	for i=1:20
		y = feval("GeneratedFunction", stuetzstellen(i));
		fitness += abs(zielwerte(i) - y);
	end %for

end %function

function fun = Terminalize(fun)
	l = size(fun, 1);
	for i=1:l
		if(i == 1)
			start = "";
		else
			start = fun(1:i-1,:);
		end %if
		if(i >= l)
			ending = "";
		else
			ending = fun(i+1:l,:);
		end %if
		if(strcmp(strtrim(fun(i,:)), "<expr>"))
			fun = [start; "0"; ending];
		elseif(strcmp(strtrim(fun(i,:)), "<pre_op>"))
			fun = [start; "sin"; ending];
		elseif(strcmp(strtrim(fun(i,:)), "<pre_2op>"))
			fun = [start; "min"; ending];
		elseif(strcmp(strtrim(fun(i,:)), "<op>"))
			fun = [start; "+"; ending];
		elseif(strcmp(strtrim(fun(i,:)), "<num>"))
			fun = [start; "0"; ending];
		elseif(strcmp(strtrim(fun(i,:)), "<var>"))
			fun = [start; "x"; ending];
		end %if
	end %for
end %function

function y = TargetFunction(x)
	y = sin(5*x)^2 + sin(2*x);
end %function

function no = CountNonTerminals(x)
	l = size(x,1);
	no = 0;
	for i=1:l
		if(isTerminal(strtrim(x(i,:))) == 0)
			no++;
		end %if
	end %for
end %function

function terminal = isTerminal(rule)
	terminal = 1;
	if(strcmp(rule, "<expr>") || strcmp(rule, "<pre_op>") || strcmp(rule, "<pre_2op>") || strcmp(rule, "<op>") || strcmp(rule, "<num>") || strcmp(rule, "<var>"))
	 terminal = 0;
	end %if
end %function

function derivate = DerivateRule (rule, c)
	derivate = [];
	if(strcmp(rule, "<expr>"))
		ruleNo = mod(c, 6);
		if(ruleNo == 0)
			derivate = ["<expr>"; "<op>"; "<expr>"];
		elseif(ruleNo == 1)
			derivate = ["<expr>"; "<op>"; "<expr>"];
		elseif(ruleNo == 2)
			derivate = ["<pre_op>"; "("; "<expr>"; ")"];
		elseif(ruleNo == 3)
			derivate = ["<pre_2op>"; "("; "<expr>"; ","; "<expr>"; ")"];
		elseif(ruleNo == 4)
			derivate = ["<num>"];
		elseif(ruleNo == 5)
			derivate = ["<var>"];
		end %if
	elseif(strcmp(rule, "<pre_op>"))
		ruleNo = mod(c, 2);
		if(ruleNo == 0)
			derivate = ["sin"];
		elseif(ruleNo == 1)
			derivate = ["exp"];
		end %if
	elseif(strcmp(rule, "<pre_2op>"))
		ruleNo = mod(c, 2);
		if(ruleNo == 0)
			derivate = ["min"];
		elseif(ruleNo == 1)
			derivate = ["divergence"];
		end %if
	elseif(strcmp(rule, "<op>"))
		ruleNo = mod(c, 3);
		if(ruleNo == 0)
			derivate = ["+"];
		elseif(ruleNo == 1)
			derivate = ["-"];
		elseif(ruleNo == 2)
			derivate = ["*"];
		end %if
	elseif(strcmp(rule, "<num>"))
		ruleNo = mod(c, 10);
		if(ruleNo == 0)
			derivate = ["0"];
		elseif(ruleNo == 1)
			derivate = ["1"];
		elseif(ruleNo == 2)
			derivate = ["2"];
		elseif(ruleNo == 3)
			derivate = ["3"];
		elseif(ruleNo == 4)
			derivate = ["4"];
		elseif(ruleNo == 5)
			derivate = ["5"];
		elseif(ruleNo == 6)
			derivate = ["6"];
		elseif(ruleNo == 7)
			derivate = ["7"];
		elseif(ruleNo == 8)
			derivate = ["8"];
		elseif(ruleNo == 9)
			derivate = ["9"];
		end %if
	elseif(strcmp(rule, "<var>"))
		ruleNo = mod(c, 1);
		if(ruleNo == 0)
			derivate = ["x"];
		end %if
	end %if
end %function

function save(fun)
	foo = "";
	for i=1:size(fun,1)
		foo = [foo, strtrim(fun(i,:))];
	end %for
	foo
	foo =  ["function y = GeneratedFunction(x)\ny=" foo ";\nend"];
	filename = "GeneratedFunction.m";
	fid = fopen (filename, "w");
	fputs (fid, foo);
	fclose (fid);
end %function