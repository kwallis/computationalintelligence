function [vnew, wnew] = OnePointCrossover (v, w)
	l = size(v, 1);
	pos = randperm(l, 1);
	v1 = v(1:pos);
	w1 = w(1:pos);
	v2 = v(pos+1:l);
	w2 = w(pos+1:l);
	vnew = [v1; w2];
	wnew = [w1; v2];
end; %function