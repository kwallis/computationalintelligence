% Test of CMA_ES
% Authors: Marco Sohm, Kevin Wallis

% Init arguments
graphics_toolkit gnuplot
more off;
randn("state", 7);

N = 40;
yP = ones(1, N);
sigmaP = 1;
my=3;
lambda = 10;
sigma_stop = 10^(-5);

funs = ['QuadraticSphereFitness'; "CigarFitness"; 'CigarTabletFitness'; 'Ellipsoid2Fitness'; 'ParabolicRidgeFitness'; 'SharpRidgeFitness'; 'DifferentPowersFitness'];

colors = ["k"; "r"; "b"; "m"; "c"; "g"];

for i=1: size(funs, 1)
  fun = deblank(funs(i, :));

  figure(i)
  clf reset;
  hold on;

  [g_counter, y_opt, m_evolution, f_evolution, mNorm_evolution, f_counter, minEigenvalue_evolution, maxEigenvalue_evolution, condition_evolution] = CMA_ES_T7(yP, sigmaP, sigma_stop, lambda, my, fun);

  disp(fun)
  disp('generations CMA_ES')
  disp(g_counter)
  disp('func eval of CMA_ES')
  disp(f_counter)
  
	semilogy(maxEigenvalue_evolution, '-', 'Color',  colors(1), 'linewidth', 2);
	semilogy(minEigenvalue_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
	semilogy(condition_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
	%semilogy(m_evolution, '-', 'Color',  colors(4), 'linewidth', 2);

  xlabel('Generations', 'FontSize', 12);
	ylabel('Max eigenvalue', 'FontSize', 12);
	ylabel('Min eigenvalue', 'FontSize', 12);
	ylabel('Conditional number K', 'FontSize', 12);
	ylabel('Mutation', 'FontSize', 12);
  title(fun, 'FontSize', 12);
  legend('max eigenvalue', 'min eigenvalue', 'condition value'); %, '\sigma evolution'); 

  hold off;
endfor
