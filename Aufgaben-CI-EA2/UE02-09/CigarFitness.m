function [fitness] = CigarFitness(y)
  y_1 = y(1)^2;
  y_2 = 0;
  for i=2:size(y)
    y_2 = y_2 + (y(i)^2);
  endfor
  fitness = y_1+1e6*y_2;
endfunction