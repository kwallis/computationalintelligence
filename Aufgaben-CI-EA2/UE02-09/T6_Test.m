% Test of CSA_ES, CMA_ES and ES_MyMy_Lambda_Sigma
% Authors: Marco Sohm, Kevin Wallis

% Init arguments
graphics_toolkit gnuplot
more off;
randn("state", 7);
 
disp("Set variable comp_mode = {'func', 'sigma', 'sigma*'} for different comparison modes");
if (exist("comp_mode", "var") == 0)
	comp_mode = "func";
endif
disp("Using comp_mode = "), disp(comp_mode);

N = 40;
yP = ones(1, N);
sigmaP = 1;
if(strcmp(comp_mode, "sigma*") == 1)
   sigmaP = 0.001;
endif
my=3;
lambda = 10;
sigma_stop = 10^(-5);

funs = ['QuadraticSphereFitness'; "CigarFitness"; 'CigarTabletFitness'; 'Ellipsoid2Fitness'; 'ParabolicRidgeFitness'; 'SharpRidgeFitness'; 'DifferentPowersFitness'];

colors = ["k"; "r"; "b"; "m"; "c"; "g"];
for i=1: size(funs, 1)
  fun = deblank(funs(i, :));

  figure(i)
  clf reset;
  hold on;
	                                                                        %CMA_ES(y_p, sigma, sigma_stop, lambda, my, fun)
  [g_counter, y_opt, m_evolution, f_evolution, mNorm_evolution, f_counter] = CMA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

  disp(fun)
  disp('generations CMA_ES')
  disp(g_counter)
  disp('func eval of CMA_ES')
  disp(f_counter)
  
  if(strcmp(comp_mode, "sigma") == 1)
    semilogy(m_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
  elseif(strcmp(comp_mode, "sigma*") == 1)
    semilogy(mNorm_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
  else
    semilogy(f_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
  endif
  
  % comparison with CSA_ES

  %[generations, y_opt, sigma_evolution, F_evolution, sigmaNorm_evolution, funcalls] = CSA_ES(yP, sigma, sigma_stop, N, lambda, my, fun)
  [gen_counter, y_opt, sigma_evol, F_evol, sigmaNorm_evolution, funcalls] = CSA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

  disp('generations CSA_ES')
  disp(gen_counter)
  disp('func eval CSA_ES')
  disp(funcalls)
  
  if(strcmp(comp_mode, "sigma") == 1)
    semilogy(sigma_evol, '-', 'Color',  colors(1), 'linewidth', 2);
  elseif(strcmp(comp_mode, "sigma*") == 1)
    semilogy(sigmaNorm_evolution, '-', 'Color',  colors(1), 'linewidth', 2);
  else
    semilogy(F_evol, '-', 'Color',  colors(1), 'linewidth', 2);
  endif

  % comparison with ES_MyMy_Lambda_Sigma
  rho=3;
  tau = 1/sqrt(2*N);

  [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter] = ES_MyMy_Lambda_Sigma(fun, yP, sigmaP, sigma_stop, N, my, rho, lambda, tau);

  disp('generations ES MyMy')
  disp(g_counter)
  disp('func eval of my/my, lambda')
  disp(f_counter)
  
  if(strcmp(comp_mode, "sigma") == 1)
    semilogy(m_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
  elseif(strcmp(comp_mode, "sigma*") == 1)
    semilogy(mNorm_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
  else
    semilogy(f_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
  endif

  xlabel('Generations', 'FontSize', 12);
  if(strcmp(comp_mode, "sigma") == 1)
    ylabel('Mutation', 'FontSize', 12);
  elseif(strcmp(comp_mode, "sigma*") == 1)
    ylabel('Normalized mutation', 'FontSize', 12);
  else
    ylabel('Fitness', 'FontSize', 12);
  endif
  title(fun, 'FontSize', 12);
  legend('(3/3_I, 10)-CMA-ES', '(3/3_I, 10)-CSA-ES', '(3/3_I, 10)-\sigmaSA-ES'); 

  hold off;
  
endfor
