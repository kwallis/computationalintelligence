% function ES_MyMy_Lambda_Sigma
% 1.My = my: size of parent population
% 2.My = rho: size of family (parents) : 1 <= 2.My <= 1.My
% Comma Selection
% Authors: Nicolaj H�ss, Martin Sobotka, Marco Sohm, Kevin Wallis

% input parameters:
% fun:          function name             [string]
% y_parental:   initial parent vector     [1 x n] 
% m_strength:   mutation strength         [real value]
% m_stop:       mutation termination      [integer value]
% n:            search dimension          [integer value]
% lambda:       lambda value              [integer value]
% tau:          tau value                 [real value]

% output parameters:
% m_evolution:  mutation over generations [n x 1]     
% g_counter:    number of generations     [integer value]
% f_evolution:  fitness values            [n x 1]
% y_optimal:    optimal vector y          [1 x n]                               
function [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter] = ES_MyMy_Lambda_Sigma(fun, y_parental, m_strength, m_stop, n, my, rho, lambda, tau)
  
  % initialize
  f_evolution = [];
  m_evolution = [];
  y_optimal = [];
  g_counter = 0;
  g_max = 10000;
  f_counter = 0;
  mNorm_evolution = [];
  
  p_population = [];
  % p_population consists of ym sm F(ym)  where ym=y_parental sm=m_strength F(ym)=fitness
  % p_population = [my x n+2] matrix
  for i=1:my
	  p_population = [p_population ; y_parental m_strength feval(fun, y_parental)];   % p_population = Bmy
  endfor
  
  f_counter += my;
  
  % (first generation) for the first loop step - originally placed inside the loop after vector initialization
  family = Marriage(p_population, rho);
  m_srenght_recomb = M_Strenght_Recombination_Average(family, rho, n);  % m_srenght_recomb = sRl
  
  do
    m_lambda = [];
    y_lambda = [];
    f_lambda = [];
    B_lambda = [];
    al = [];
    
    % do this outside of the lambda loop because its the same for all in MyMyI algorithm
    
    % select rho parents(family) from parent population p_population 
    % in MyMyI select all parents
    %family = Marriage(p_population, rho);
    % recombination mutation strength 
    %m_srenght_recomb = M_Strenght_Recombination_Average(family, rho, n);  % m_srenght_recomb = sRl
    % recombination object parameters 
    y_recomb = Y_Recombination_Average(family, rho, n);                   % y_recomb = yRl
    
    % mutation over generations
    m_evolution = [m_evolution ; m_srenght_recomb];
    % fitness over generations, average
    f_evolution =  [f_evolution ; feval(fun, y_recomb)];
    % normalized mutation
    mNorm_evolution =  [mNorm_evolution; m_srenght_recomb/norm(y_recomb) * n];
    
    for l=1:lambda  
      xi = e ^(tau * randn());
	    % mutate strategy parameter = mutation strength
      m_lambda = [m_lambda ; xi * m_srenght_recomb];                      % m_lambda = sl
      % mutate object parameter
      y_lambda = [y_lambda ; RealOffspring(y_recomb, m_lambda(l))];       % y_lambda = yl
      % calc fitness
      f_lambda = [f_lambda ; feval(fun, y_lambda(l, :))];                 % f_lambda = Fl
      % individual consists of object param, sigma, fitness
      al = [y_lambda(l, :) m_lambda(l) f_lambda(l)];
      B_lambda = [B_lambda ; al];     	  
    endfor
	
	f_counter += lambda;
	
    % select my best offsprings of B_lambda = p_population(B_my):
    p_population = [];
    
    for i=1:my
      indexBestValue = find(f_lambda == min(f_lambda));
    
      Bmy_m_strength = m_lambda(indexBestValue);
      Bmy_fitness = f_lambda(indexBestValue);
      Bmy_y = y_lambda(indexBestValue, :);
     
      p_population = [p_population ; Bmy_y Bmy_m_strength Bmy_fitness];   % p_population = Bmy
      
      f_lambda(indexBestValue)=inf;    
      % set the current best f_lambda value to inf get the second best f_lambda value in the next iteration
      % f_lambda can be changed because its not needed anymore
    endfor
  
    g_counter++;
	% new generation; this two lines were originally placed inside the loop after vector initialization
	family = Marriage(p_population, rho);
    m_srenght_recomb = M_Strenght_Recombination_Average(family, rho, n);  % m_srenght_recomb = sRl
  until (m_srenght_recomb < m_stop || g_counter > g_max)
  
  y_optimal = Y_Recombination_Average(p_population, rho, n);
endfunction

% Marriage function
% See slide 99
% only for extensions, we only need rho == my
function [family] = Marriage(parent_pool, rho)
  family = parent_pool;
  %my = size(parent_pool, 2);
	%if (rho < my)
		% TODO (if needed) pick randomly rho parents from the pool and return it
	%else if (rho == my) % MyMyI
	%	family = parent_pool
	%else
		% rho > my
		% Illegal case
	%endif
endfunction

% function to create offsprings
function [y_offspring] = RealOffspring(y_parental, m_strength)
  mutation = randn(size(y_parental,2),1);
  y_offspring = y_parental + m_strength*(mutation');
endfunction

% recombination functions
function [avg] = Y_Recombination_Average(family, rho, n)
  tmp = family;
  tmp(:,[n+1,n+2]) = [];    % remove last two rows of family matrix to get rid of m_strenght and fitness
  avg = Average(tmp, rho);  % tmp is y_parental matrix
endfunction

function [avg] = M_Strenght_Recombination_Average(family, rho, n)
  avg = Average(family(:, n+1), rho);  % family(:, n+1) select all rows and the second column of family matrix
                                % in column n+1 is m_strength=sm, transpone it to get column vector
endfunction

function [avg] = Average(family, rho)
  [K, M] = size(family);   
  avg = zeros(1, M);
  
  % calculate arithmetic mean
  for i = 1:M   
      avg(i) = sum(family(:,i)) / rho;
  end
endfunction