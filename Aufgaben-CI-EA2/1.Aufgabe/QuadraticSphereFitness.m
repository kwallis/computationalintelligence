function [fitness] = QuadraticSphereFitness(y)
  fitness = abs(y * y');
endfunction