% Authors: Marco Sohm, Kevin Wallis
more off;
randn(7);

dims = [4; 8; 16; 32; 64];
funs = char('QuadraticSphereFitness', 'Ellipsoid1Fitness');

sigmaP = 1;
my = 3;
rho = 3;
lambda = 12;
F_target = 1e-4;  
trials = 15;

colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];

N = size(funs, 1);
ERT_over_N = zeros(1,N);
Ps_over_N = zeros(1,N);

figOffset = 1;
for i=1:N
  figure(figOffset)
  figOffset = figOffset + 1;
  clf reset;
  hold on;
  
  fun = funs(i, :); 
  fun = deblank(fun);                                                                                                                             
  disp('fun')
  disp(fun)
  
  for j=1:size(dims, 1)
    N = dims(j);
    disp('dimension')
    disp(N)
    yP = 10 * ones(1, N);
    tau = 1 / sqrt(2 * N);
    
    ERT_over_N(j) = 0;
    E_ru = 0;
    E_rs = 0;
    
    nr_Succ = 0;        % number of successfull trials
    nr_Unsucc = 0;      % number of successfull trials
    nr_FE_Succ = 0;     % number of function evalutions in successfull trials
    nr_FE_Unsucc = 0;   % number of function evalutions in unsuccessfull trials
    
    for k=1:trials
                                                                                            % ES_MyMy_Lambda_Sigma(fun, y_parental, m_strength, n, my, rho, lambda, tau, F_target)
      [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter, success] = ES_MyMy_Lambda_Sigma(fun, yP, sigmaP, N, my, rho, lambda, tau, F_target);
      if(success) 
        nr_Succ = nr_Succ + 1;
        nr_FE_Succ = nr_FE_Succ + f_counter;
      else 
        nr_FE_Unsucc = nr_FE_Unsucc + f_counter; 
      end
      msg = strcat(int2str(k), '. trial done');
      disp(msg)
    end
    nr_Unsucc = trials - nr_Succ;
    
    Ps = nr_Succ / trials;                % 2.12 slide 52
    if(nr_Unsucc ~= 0)
      E_ru = nr_FE_Unsucc / nr_Unsucc;    % 2.13 slide 52
    end
    if(nr_Succ == 0)
      disp('No successfull computation -> ERT cannot be calculated!')
      ERT = 0;
    else
      E_rs = nr_FE_Succ / nr_Succ;        % 2.13 slide 52
      ERT = (1 / Ps - 1) * E_ru + E_rs;     % 2.11 slide 51
    end
    
    
    ERT_over_N(j) = ERT;
    Ps_over_N(j) = Ps;
  end
  % TODO plot anpassen
  semilogy(dims, ERT_over_N, '-', 'Color',  colors(3), 'linewidth', 2); % TODO doppelt logarithmisch?!
  xlabel('N', 'FontSize', 12);
  ylabel('ERT', 'FontSize', 12 );

  title(['(3/3_I, 12)-\sigmaSA-ES; fun=' fun], 'FontSize', 12);
  legend('ERT = f(N)'); 

  hold off;
  tmp = strcat('./figures/Task1_', fun, '.png');
  print(tmp, '-dpng');
  
  figure(figOffset)
  figOffset = figOffset + 1;
  clf reset;
  hold on;
  plot(dims, Ps_over_N, '-', 'Color',  colors(4), 'linewidth', 2);
  xlabel('N', 'FontSize', 12);
  ylabel('Ps', 'FontSize', 12 );

  title(['(3/3_I, 12)-\sigmaSA-ES; fun=' fun], 'FontSize', 12);
  legend('Ps = f(N)'); 

  hold off;
  tmp = strcat('./figures/Task1_Ps_', fun, '.png');
  print(tmp, '-dpng');
end
