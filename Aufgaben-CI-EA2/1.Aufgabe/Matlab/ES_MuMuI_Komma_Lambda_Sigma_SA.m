% function ES_MuMuI_Komma_Lambda_Sigma_SA
% Comma Selection

function [g, y, hist] = ES_MuMuI_Komma_Lambda_Sigma_SA(fitness_fun, y, sigma, sigma_stop, mu, lambda, tau)

  % initialize
  y = y';
  n = size(y, 1);
  hist.f = [];
  hist.sigma = [];
  hist.sigma_norm = [];
  g = 0;
  
  p_pop = [];
  % p_pop consists of y_m s_m F(y_m)
  % p_pop = [mu x n+2] matrix
  for i=1:mu
	  p_pop(i).y = y;
	  p_pop(i).sigma = sigma;
	  p_pop(i).f = feval(fitness_fun, y');
  end
  
  % (first generation) for the first loop step - originally placed inside the loop after vector initialization
  sigma = 1/mu * sum([p_pop(:).sigma]);
  y = 1/mu * sum([p_pop(:).y]')';
  
  while (sigma >= sigma_stop)
    
    % collect history data
    hist.sigma(g + 1) = sigma;
	hist.sigma_norm(g + 1) = (sigma/norm(y) * n);
	hist.f(g + 1) =  feval(fitness_fun, y');
	
	% generate lambda offsprings
	for l=1:lambda
      xi = exp(tau * randn());
      offspring(l).sigma = xi * sigma;
      offspring(l).y = y + offspring(l).sigma * randn(n, 1);
      offspring(l).f = feval(fitness_fun, offspring(l).y');
    end
	
	% sort offspring by fitness value
	[~, order] = sort([offspring(:).f], 'ascend');
	p_pop = offspring(order);
	
	sigma = 1/mu * sum([p_pop(1:mu).sigma]);
	y = 1/mu * sum([p_pop(1:mu).y]')';
	
    g = g + 1;
  end
end

