function [fitness] = Ellipsoid1Fitness(y)
  fitness = 0;
  N = size(y, 2);
  for i=1:N
    fitness = fitness + i * y(i)^2;  
  end
end