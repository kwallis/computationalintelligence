% Authors: Marco Sohm, Kevin Wallis
graphics_toolkit gnuplot
more off;
randn("state", 7);

dims = [4; 8; 16; 32; 64];
funs = ['QuadraticSphereFitness'; 'Ellipsoid1Fitness'];

sigmaP = 1;
my = 3;
rho = 3;
lambda = 12;
sigma_stop = 1e-5;
F_target = 1e-4;  
trials = 15;

colors = ["k"; "r"; "b"; "m"; "c"; "g"];

ERT_over_N = [];
Ps_over_N = [];

figOffset = 1;
for i=1:size(funs, 1)
  figure(figOffset)
  figOffset++;
  clf reset;
  hold on;
  
  fun = funs(i, :); 
  fun = deblank(fun);                                                                                                                             
  disp('fun')
  disp(fun)
  
  for j=1:size(dims, 1)
    N = dims(j);
    disp('dimension')
    disp(N)
    yP = 10 * ones(1, N);
    tau = 1 / sqrt(2 * N);
    
    ERT_over_N(j) = 0;    
       
    % function [ert, ps] = ERT(func, fun, yP, sigmaP, sigma_stop, N, my, rho, lambda, tau, F_target)  
    [ert, ps] = feval('ERT','ES_MyMy_Lambda_Sigma', trials, fun, yP, sigmaP, sigma_stop, N, my, rho, lambda, tau, F_target); 
    ERT_over_N(j) = ert;
    Ps_over_N(j) = ps;
  endfor
  
  
  
  % TODO plot anpassen
  semilogy(dims, ERT_over_N, '-', 'Color',  colors(3), 'linewidth', 2); % TODO doppelt logarithmisch?!
  xlabel('N', 'FontSize', 12);
  ylabel('ERT', 'FontSize', 12 );

  title(['(3/3_I, 12)-\sigmaSA-ES; fun=' fun], 'FontSize', 12);
  legend('ERT = f(N)'); 

  hold off;
  tmp = strcat('./figures/Task1_', fun, '.png');
  print(tmp, '-dpng');
  
  figure(figOffset)
  figOffset++;
  clf reset;
  hold on;
  plot(dims, Ps_over_N, '-', 'Color',  colors(4), 'linewidth', 2);
  xlabel('N', 'FontSize', 12);
  ylabel('Ps', 'FontSize', 12 );

  title(['(3/3_I, 12)-\sigmaSA-ES; fun=' fun], 'FontSize', 12);
  legend('Ps = f(N)'); 

  hold off;
  tmp = strcat('./figures/Task1_Ps_', fun, '.png');
  print(tmp, '-dpng');
endfor