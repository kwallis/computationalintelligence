more off;

N = [5;1];
mu = 100;
p_c = 0.6;
colors = ['r' 'b'];

figure(1)
hold on;

for i=1:2
    [g_counter, avg_fitness] = CGA(N(i), mu, p_c);
    plot(avg_fitness, 'Color', colors(i));
    
    % values
    disp(['Generations for N = ' num2str(N(i))]);
    disp(g_counter);
end

% plot settings
xlabel('generations');
ylabel('average fitness');
legend(['Average fitness N = ' num2str(N(1))], ['Average fitness N = ' num2str(N(2))]);
title('CGA Task 12');
ylim([0 0.7]);

hold off;