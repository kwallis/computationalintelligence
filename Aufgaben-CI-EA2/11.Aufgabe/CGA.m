function [g_counter, fitness_avg] = CGA(mu, N, p_m, p_c)
    g_max = 200;

    fitness_avg = [];
    g_counter = 0;
    
    % Init X
    X =  randi([0 1], mu, N);

    while (g_counter < g_max)
        F = zeros(1, mu);
        p = zeros(1, mu);
        phi = zeros(1, mu);
        phi_sum = 0;

        for j=1:mu
            F(j) = OneMax(X(j,:));
            phi(j) = F(j);			% fitness-proportional
            phi_sum = phi_sum + phi(j);
        end

        for j=1:mu
            p(j) = phi(j) / phi_sum;
        end

        X_temp = X;
        for j=1:2:mu
            V = PropSelect(X_temp , p);

            %W = V;
            %while (sum(W ~= V) == 0)
            W = PropSelect(X_temp , p);
            %end 

            if (rand() < p_c)
                [A, B] = Crossover(V,W);
                X(j,:) = A;
                X(j+1,:) = B;
            else
                X(j,:) = V;
                X(j+1,:) = W;
            end
        end

        fitness_sum = 0;
        for j=1:mu
            % Mutate Xj
            X(j, :) = Mutate(X(j,:), p_m);
            fitness_sum = fitness_sum + OneMax(X(j,:));
        end

        fitness_avg = [ fitness_avg ; (fitness_sum / mu) ];
        g_counter = g_counter + 1;
    end

end

function Y = PropSelect(X, p)
    r = rand();
    P = 0;
    j = 0;

    while (P < r)
        j = j + 1;
        P = P + p(j);
    end
    
    Y = X(j,:);
end

function Y = Mutate(X, p_m)
    len = size(X, 2);
    r = rand(len, 1)';
    m = r < p_m;
    Y = X + m;
    Y = mod(Y, 2);
end

function fitness = OneMax(X)
    fitness = sum(X);
end

function [A,B] = Crossover(V,W)
    len = size(V,2);
    point = rand() * len;

    A = V;
    B = W;

    for j=1:len
        if (j < point)
            B(j) = V(j);
            A(j) = W(j);
        end
    end
end