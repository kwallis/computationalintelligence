more off;

mu = 60;
l = 20;
p_c = 0.6;
p_m = [0.001 0.01];
colors = ['r' 'b'];

figure(1)
hold on;

for i=1:2
    [g_counter, avg_fitness] = CGA(mu, l, p_m(i), p_c);
    plot(avg_fitness, 'Color', colors(i));
    
    % values
    disp(['Generations for p_m = ' num2str(p_m(i))]);
    disp(g_counter);
end

% plot settings
xlabel('generations');
ylabel('average fitness');
legend(['Average fitness p_m = ' num2str(p_m(1))], ['Average fitness p_m = ' num2str(p_m(2))]);
title('CGA Task 11');
ylim([0 22]);
hold off;