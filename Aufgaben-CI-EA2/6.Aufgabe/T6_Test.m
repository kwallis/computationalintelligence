% Authors: Marco Sohm, Kevin Wallis
global city_pos;
global city_count;
more off;

colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];

cities = ['cities_A_positions.dat'; 'cities_B_positions.dat'];
np = [24, 25];
T_init = 50;
T_stop = 1e-8;
L = 25;         % about number of cities
alpha = 0.9;    % alpha btw 0 and 1

for i=1:size(cities)
    figure(i)
    clf reset;
    hold on;

    fnr = fopen(cities(i, :), 'r'); % TODO richtig eingelesen?
    city_pos = fscanf(fnr, '%f', [np(i), 2]);
    fclose(fnr);
    city_count = np(i);
    
    y_p = zeros(np(i), 1);
    for k=1:np(i)
        y_p(k, 1) = k;     % order of visited cities
    end
    
    run = i + size(np, 2);
    % [g_counter, f_evolution, y_p] = SimulateAnnealing(y_p, T_init, T_stop, L, alpha, run) 
    [g_counter, f_evolution, y_p] = SimulateAnnealing(y_p, T_init, T_stop, L, alpha, run);
    
    figure(i);
    disp('generations SimulateAnnealing')
    disp(g_counter)
    plot(f_evolution, '-', 'Color',  colors(i, :), 'linewidth', 2);

    xlabel('Generations', 'FontSize', 12);
    ylabel('Fitness', 'FontSize', 12 );
    title(['SimulateAnnealing with ' cities(i, :)], 'FontSize', 12);
    legend('Fitness'); 

    hold off;
    tmp = strcat('./figures/SimulateAnnealing_', cities(i, :), '.png');
    print(tmp, '-dpng')
end

