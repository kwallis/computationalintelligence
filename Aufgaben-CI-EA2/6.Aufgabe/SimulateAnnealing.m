% function SimulateAnnealing

% input parameters:
% y_p:          order of visited cities             [#ofCities x 1] 
% T_init:       temperature init                    [real value]
% T_stop:       temperature stop                    [integer value]
% L:            # of iterations with same temp.     [integer value]
% fun:          function to be evaluated            [integer value]
% run:          just for figure number              [integer value]

% output parameters:
% g_counter:    number of generations     [integer value]
% f_evolution:  fitness values            [n x 1]
% y_p:          best found solution       [#ofCities x 1]

function [g_counter, f_evolution, y_p] = SimulateAnnealing(y_p, T_init, T_stop, L, alpha, run)    
    F_p = CalcTotalDistance(y_p);
    T = T_init;
    % TODO - adaptives Verfahren, das die Temperatur am Anfang genugend
    % hochfaehrt(oder von au�en zufaellig initialisieren?!)
    g_counter = 0;
    g_max = 15000;
    f_evolution = [];
    
    figure(run)
    plotIt(y_p);
    
    while(T > T_stop && g_counter < g_max)
        y_tilde = Lin_2_Opt(y_p);
        F_tilde = CalcTotalDistance(y_tilde);

        if (F_tilde <= F_p || exp((F_p - F_tilde) / T) > rand())
            y_p = y_tilde;
            F_p = F_tilde;            
            % if the tour has improved or is accepted - plot it
            plotIt(y_p);
        end

        if(mod(g_counter, L) == (L-1))
            T = alpha * T;
        end
        g_counter = g_counter + 1;
        f_evolution = [f_evolution; F_p];
    end
    xlabel('x', 'FontSize', 12);
    ylabel('y', 'FontSize', 12 );
    title('Cities', 'FontSize', 12);
    legend('Cities'); 

    hold off;
    tmp = strcat('./figures/Solution_Cities_', int2str(run), '.png');
    print(tmp, '-dpng')
end

function y_p = Lin_2_Opt(y_p) % according to slide 65
    global city_count;
    % select 2 random cities
    city_1 = 0;
    city_2 = 0;
    while(city_1 == 0)
        city_1 = round(city_count * rand()); % city_1 could become 0
    end
    while(city_2 == 0 || city_1 == city_2)
        city_2 = round(city_count * rand());
    end
    %  swap order of visit by cutting connections
    tmp = fliplr(y_p(city_1: city_2)')';
	% or use http://de.mathworks.com/help/matlab/ref/flipud.html instead of fliplr
    y_p(city_1: city_2) = tmp;
end

function F_tilde = CalcTotalDistance(y_p)
    global city_pos;
    global city_count;
    F_tilde = 0;
    for i=1:(city_count - 1)
        F_tilde = F_tilde + pdist([city_pos(y_p(i), 1), city_pos(y_p(i), 2); city_pos(y_p(i+1), 1), city_pos(y_p(i+1), 2)], 'euclidean');
    end
    F_tilde = F_tilde + pdist([city_pos(y_p(city_count), 1), city_pos(y_p(city_count), 2); city_pos(y_p(1), 1), city_pos(y_p(1), 2)], 'euclidean'); % last with first city
end

function plotIt(y_p)
    global city_pos;
    global city_count;
    clf;
    hold on;
    axis([0 6 0 6]);
    plot(city_pos(y_p(:), 1), city_pos(y_p(:), 2), '*');
    for i=1:(city_count - 1)
        line([city_pos(y_p(i), 1), city_pos(y_p(i+1), 1)], [city_pos(y_p(i), 2), city_pos(y_p(i+1), 2)]);
    end
    line([city_pos(y_p(city_count), 1), city_pos(y_p(1), 1)], [city_pos(y_p(city_count), 2), city_pos(y_p(1), 2)]);
    
    drawnow;
    hold off;
end