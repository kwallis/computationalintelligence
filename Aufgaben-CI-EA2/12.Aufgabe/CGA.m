function [g_counter, fitness_avg] = CGA(N, mu, p_c)
g_max = 50;
L = 3;

l = N * L;
p_m = 1 / (mu * l);
fitness_avg = [];
g_counter = 0;
y = [];

% Init X
X = randi([0 1], mu, l);

while (g_counter < g_max)
    F = zeros(1, mu);
    p = zeros(1, mu);
    phi = zeros(1, mu);
    phi_sum = 0;
    
    for j=1:mu
        y = GPMapping(X(j,:), L);
        F(j) = Task12(y);
        phi(j) = F(j);			% fitness-proportional
        phi_sum = phi_sum + phi(j);
    end
    
    for j=1:mu
        p(j) = phi(j) / phi_sum;
    end
    
    X_temp = X;
    for j=1:2:mu
        V = PropSelect(X_temp , p);
        %W = V;
        %while (sum(W ~= V) == 0)
        W = PropSelect(X_temp , p);
        %end
        
        if (rand() < p_c)
            [A, B] = Crossover(V,W);
            X(j,:) = A;
            X(j+1,:) = B;
        else
            X(j,:) = V;
            X(j+1,:) = W;
        end
    end
    
    fitness_sum = 0;
    for j=1:mu
        % Mutate Xj
        X(j, :) = Mutate(X(j,:), p_m);
        fitness_sum = fitness_sum + Task12(GPMapping(X(j,:), L));
    end
    
    fitness_avg = [ fitness_avg ; (fitness_sum / mu) ];
    g_counter = g_counter + 1;
    
    percent = g_counter * 100 / g_max;
    if (mod(percent, 10) == 0)
        disp(percent);
    end
end

disp('Calculated y');
y_sum = GPMapping(X(1,:), L);
for i=2:mu
    y_sum = y_sum + GPMapping(X(i,:), L);
end
disp(y_sum / mu);
end

function Y = PropSelect(X, p)
r = rand();
P = 0;
j = 0;

while (P < r)
    j = j + 1;
    P = P + p(j);
end

Y = X(j,:);

end

function fitness = Task12(y)
N = size(y,1);
s_y = 0;
for i=1:N
    s_y = s_y + (y(i) - i) ^ 2;
end
fitness = 1/(1 + s_y);
end

function y = GPMapping(b, L)
l = size(b, 2);
y = zeros(l / L, 1);

cnt = 0;
for i=1:L:(l-L+1)
    cnt = cnt + 1;
    y(cnt) = BinaryToInteger(b, i, (i+L));
end

end

function result = BinaryToInteger(b, start_bit, end_bit)
L = end_bit - start_bit;
result = 0;
for i=0:(L-1);
    result = result + b(start_bit + i) * 2 ^ (L - i - 1);
end
end

function Y = Mutate(X, p_m)
len = size(X, 2);

r = rand(len, 1)';
m = r < p_m;

Y = X + m;

Y = mod(Y, 2);
end

function fitness = OneMax(X)
fitness = sum(X);
end

function [A,B] = Crossover(V,W)
len = size(V,2);
point = rand() * len;

A = V;
B = W;

for j=1:len
    if (j < point)
        B(j) = V(j);
        A(j) = W(j);
    end
end

end