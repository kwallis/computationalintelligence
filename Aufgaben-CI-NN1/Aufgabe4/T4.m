more off;

x = [1,2; 2,1; 3,3; 4,1; 4,9; 2,10; 3,9; 4,8; 5,8; 6,9; 7,5; 6,2]';
d = [1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1];
C = 2;
a = 0.1;
w_end = 1e-8;
max_iter = 100000;
[w, number_weightchanges, ni, nmis, w_changes] = alphaLMS(x, d, a, C, w_end, max_iter);

figure(1);
semilogy(nmis);
figure(4);
plot(nmis);

figure(2);
plot(w_changes);

figure(3);
classDomains = zeros(9, 9);
%classDomains = zeros(800, 900); %f�r h�here Aufl�sung
for xCoord = 1 : 9
%for xCoord = 1 : 900
    for yCoord = 1 : 9
    %for yCoord = 1 : 800
        c = sign(w'*[xCoord, yCoord, 1]');
        %[maxClassWeight, c] = max((w(:,1:4)'*[xCoord/100 yCoord/100 1]')');
        classDomains(xCoord, yCoord) = c;
    end
end

imagesc(classDomains);
colours = ['k', 'r', 'w', 'g'];
hold on;

for i=1:size(x,2)
    colour = d(i);
    if colour == -1
        colour = 2;
    end
    plot(x(2,i), x(1,i), 'Marker', 'o', 'Color', colours(colour), 'MarkerFaceColor', colours(colour));
    %f�r h�here Aufl�sung
    %plot(x(2,i)*100, x(1,i)*100, 'Marker', 'o', 'Color', colours(d(i)), 'MarkerFaceColor', colours(d(i)));
end;
