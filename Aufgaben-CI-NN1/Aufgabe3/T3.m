more off;

x = [ 1 2; 2 1; 2 2; 2 3; 3 1; 3 2; 3 4; 4 5; 5 4; 6 1; 6 2; 7 1; 7 3; 8 2; 2 7; 6 7]';
d = [ 1 1 1 1 1 1 2 2 2 3 3 3 3 3 4 4];
C = 4;
max_iter = 10000;
[w, number_weightchanges, ni, nmis] = PerceptronLearning(x, d, C, max_iter);

figure(1);
plot(nmis);

figure(2);
classDomains = zeros(8, 9);
%classDomains = zeros(800, 900); %f�r h�here Aufl�sung
for xCoord = 1 : 9
%for xCoord = 1 : 900
    for yCoord = 1 : 8
    %for yCoord = 1 : 800
        [maxClassWeight, c] = max((w(:,1:4)'*[xCoord yCoord 1]')');
        %[maxClassWeight, c] = max((w(:,1:4)'*[xCoord/100 yCoord/100 1]')');
        classDomains(xCoord, yCoord) = c;
    end
end

imagesc(classDomains);
colours = ['g', 'r', 'w', 'k'];
hold on;

for i=1:size(x,2)
    plot(x(2,i), x(1,i), 'Marker', 'o', 'Color', colours(d(i)), 'MarkerFaceColor', colours(d(i)));
    %f�r h�here Aufl�sung
    %plot(x(2,i)*100, x(1,i)*100, 'Marker', 'o', 'Color', colours(d(i)), 'MarkerFaceColor', colours(d(i)));
end;

% plot settings
xlabel('x');
ylabel('y');
title('NN Task 3');
hold off;