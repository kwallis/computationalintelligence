% Name: alphaLMS
% Author: Philipp Ritter
%--------------------------------------------------------------------------
%| Input parameters                                                       |
%--------------------------------------------------------------------------
%| Name                | Type        | Description                        |
%--------------------------------------------------------------------------
%| x                   | [K x M]     | K roW vectors of dimension M       |
%| d                   | [K]         | target values e.g. {-1,1}^K        |
%| a                   | Integer     | alpha					          |
%| C                   | Integer     | number of Classes                  |
%| w_end               | Integer     | minimal weight change	          |
%| max_iter            | Integer     | Max number of iterations           |
%--------------------------------------------------------------------------
%| Output parameters                                                      |
%--------------------------------------------------------------------------
%| Name                | Type        | Description                        |
%--------------------------------------------------------------------------
%| W                   | [M+1]       | Weight vector                      |
%| theta               | Integer     | the M+1st component of W           |
%| n_W_changes         | Integer     | number of Weight changes           |
%| n_iter              | Integer     | number of for-loop cycles          |
%| n_misclassified     | [N, n_iter] | number of misclassified data       |
%--------------------------------------------------------------------------
function [W, n_W_changes, n_iter, n_misclassified, w_changes] = alphaLMS(x, d, a, C, w_end, max_iter)
% Initialize parameters
% K ... number of input data
% M ... dimension of the input data
[M, K] = size(x);
% Weight vector With dimension M+1st because of the theta
% W = (M+1) x C matrix
W = rand(M+1, 1);
phi = x;
% add to each input data a theta as M+1st parameter
phi(M+1,:)=ones(1,K);
n_W_changes = 0;
n_misclassified = [];

n_iter = 1;
w_change = inf;
w_changes=[];
while(w_change > w_end && n_iter <= max_iter)
    n_miscl = 0; 
	W_old = W;
    for k=1:K 
		%calculate class
         c = sign(W'*phi(:,k));
         % check if class and desired class match
         if (c ~= d(k))
             % increase weight for 
			 W = W - a * phi(:,k)/((phi(:,k)'*phi(:,k))^2) * (W'*phi(:,k)-d(k));
             n_W_changes = n_W_changes + 1;
             n_miscl = n_miscl + 1;
         end
    end 
    n_misclassified(n_iter) = n_miscl;
	w_change = W_old - W;
	w_change = w_change'*w_change;
	w_changes = [w_changes, w_change];
    if (n_miscl == 0) 
        break; 
    end
	n_iter = n_iter + 1;
end
W = W/norm(W);
end