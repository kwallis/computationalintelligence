% Name: PerceptronLearning
% Author: Marco Sohm, Kevin Wallis
%--------------------------------------------------------------------------
%| Input parameters                                                       |
%--------------------------------------------------------------------------
%| Name                | Type        | Description                        |
%--------------------------------------------------------------------------
%| x                   | [K x M]     | K roW vectors of dimension M       |
%| d                   | [K]         | target values e.g. {-1,1}^K        |
%| C                   | Integer     | number of Classes                  |
%| max_iter            | Integer     | Max number of iterations           |
%--------------------------------------------------------------------------
%| Output parameters                                                      |
%--------------------------------------------------------------------------
%| Name                | Type        | Description                        |
%--------------------------------------------------------------------------
%| W                   | [M+1]       | Weight vector                      |
%| theta               | Integer     | the M+1st component of W           |
%| n_W_changes         | Integer     | number of Weight changes           |
%| n_iter              | Integer     | number of for-loop cycles          |
%| n_misclassified     | [N, n_iter] | number of misclassified data       |
%--------------------------------------------------------------------------
function [W, n_W_changes, n_iter, n_misclassified] = PerceptronLearning(x, d, C, max_iter)
% Initialize parameters
% K ... number of input data
% M ... dimension of the input data
[M, K] = size(x);
% Weight vector With dimension M+1st because of the theta
% W = (M+1) x C matrix
W = rand(M+1, C);
phi = x;
% add to each input data a theta as M+1st parameter
phi(M+1,:)=ones(1,K);
n_W_changes = 0;
n_misclassified = [];

for n_iter=1:max_iter
    n_miscl = 0; 
    for k=1:K 
         % calculate classWeights for each class and then
         % take argmax of classWeights, this is the class for this vec
		 [maxClassWeight, c] = max((W'*phi(:,k))');
         % check if class and desired class match
         class = d(k);
         if class == -1
             class = 2;
         end
         if (c ~= class)
             % increase weight for 
             W(:,class) = W(:,class) + phi(:, k);
             W(:,c) = W(:,c) - phi(:, k);
             n_W_changes = n_W_changes + 1;
             n_miscl = n_miscl + 1;
         end
    end 
    n_misclassified(n_iter) = n_miscl;
    if (n_miscl == 0) 
        break; 
    end
end
W = W/norm(W);
end