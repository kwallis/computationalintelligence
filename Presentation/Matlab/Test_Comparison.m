% Authors: Marco Sohm, Kevin Wallis
clear all;
global N;
more off;
randn('state', 7);

colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];

% PSO
useConstrictionFactor = 1;  
if(useConstrictionFactor)
    C1 = 2.05;
    C2 = 2.05;
else
    C1 = 1.494;
    C2 = 1.494;
end
N = 2;
omega = 0.7;
Vmax = 3;
swarmSize = 10;
useLocalNeighborhood = 0;  % implemented as social ring topology - social: not based on physical distances btw particles
F_target = 1e-7;

% other ES    

% same init as PSO.m: particles(:, :, POSITION) = 5 * ones(N, swarmSize) + 10 * randn(N, swarmSize)
yP = 5 * ones(1, N) + 10 * randn(1, N);
sigmaP = 1;
my=3;
lambda = 10;

funsData = ['QuadraticSphereFitness'; 'SchwefelFitness       '; 'RastriginFitness      '; 'RosenbrockFitness     '];
funs = cellstr(funsData);

for i=1: size(funs, 1)

    fun = funs{i}; 
    fun = deblank(fun);
    disp('fun')
    disp(fun)
  
    if(useConstrictionFactor)
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness)
        [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, 0, 1);
    else
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
        [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 0, 1);
    end
    
    figure(i)
    clf reset;
    hold on;
    
    disp('generations PSO')
    disp(g_counter)
    disp('func eval PSO')
    disp(f_counter)

    semilogy(f_evolution(~isnan(f_evolution)), '-', 'Color',  colors(1), 'linewidth', 2);
    % http://www.mathworks.com/matlabcentral/answers/95921-why-does-the-semilogy-function-not-plot-onto-a-logarithmic-scale-in-matlab-6-5-r13

                                                                            % CMA_ES(fun, y_p, sigma, my, lambda, F_target)
    [g_counter, y_opt, m_evolution, f_evolution, mNorm_evolution, f_counter] = CMA_ES(fun, yP, sigmaP, my, lambda, F_target);

    disp('generations CMA_ES')
    disp(g_counter)
    disp('func eval of CMA_ES')
    disp(f_counter)
    
    semilogy(f_evolution, '-', 'Color',  colors(2), 'linewidth', 2);

    % comparison with ES_MyMy_Lambda_Sigma
    rho = 3;
    tau = 1 / sqrt(2 * N);
                                                                               % ES_MyMy_Lambda_Sigma(fun, y_parental, m_stop, my, rho, lambda, tau, F_target)
    [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter] = ES_MyMy_Lambda_Sigma(fun, yP, sigmaP, my, rho, lambda, tau, F_target);

    disp('generations ES MyMy')
    disp(g_counter)
    disp('func eval ES MyMy')
    disp(f_counter)

    semilogy(f_evolution, '-', 'Color',  colors(4), 'linewidth', 2);


    xlabel('Generations', 'FontSize', 12);
    ylabel('Fitness', 'FontSize', 12);

    set(gca,'yscale','log');
    title(['PSO, CMA-ES and \sigmaSA-ES comparision; N=' int2str(N) '; fun=' fun], 'FontSize', 12);
    legend('PSO', '(3/3_I, 10)-CMA-ES', '(3/3_)I, 10)-\sigmaSA-ES'); 

    hold off;

    tmp = strcat('./figures/Comparison_Fitness', fun, '.png');

    print(tmp, '-dpng');
    disp('-------------------------------------------------')
 end
