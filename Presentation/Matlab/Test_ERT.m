% Authors: Marco Sohm, Kevin Wallis
clear all;
global N;
more off;
randn(7);

dims = [4; 8; 16; 32; 64; 128];
% funs = char('QuadraticSphereFitness', 'SchwefelFitness');
funs = char('QuadraticSphereFitness');
ESfuns = char('CMA_ES', 'PSO_ConstrictionFactor', 'ES_MyMy_Lambda_Sigma'); 

% for PSO the default params are used - see PSO.m

% params for CMA_ES and ES_MyMy_Lambda_Sigma
sigmaP = 1;
my = 3;
rho = 3;
lambda = 12;
F_target = 1e-3;  % F_target in PSO must be the same!
trials = 10;

colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];

figOffset = 1;

for i=1:size(funs, 1)
    fun = funs(i, :); 
    fun = deblank(fun);                                                                                                                             
    disp('fun')
    disp(fun)

    for k=1:size(ESfuns, 1)
        ESfun = ESfuns(k, :); 
        ESfun = deblank(ESfun);
        
        ERT_over_N = zeros(1, size(dims, 1));
        Ps_over_N = zeros(1, size(dims, 1));

        for j=1:size(dims, 1)
            N = dims(j);
            disp('dimension')
            disp(N)
            yP = 5 * ones(1, N) + 10 * randn(1, N);
            tau = 1 / sqrt(2 * N);

            % [ert, ps] = ERT(ESfun, Fitfun, trials, y, sigma, my, rho, lambda, tau, F_target)     
            [ert, ps] = ERT(ESfun, fun, trials, yP, sigmaP, my, rho, lambda, tau, F_target); 

            ERT_over_N(j) = ert;
            Ps_over_N(j) = ps;
        end
        % TODO plot anpassen
        figure(figOffset)
        hold on;
        loglog(dims, ERT_over_N, '-', 'Color',  colors(i+k), 'linewidth', 2);
        
        figure(figOffset + 1)
        hold on;
        plot(dims, Ps_over_N, '-', 'Color',  colors(i+k), 'linewidth', 2);
    end

    figure(figOffset)
    hold off;
    set(gca,'yscale','log');
    set(gca,'xscale','log');
    xlabel('N', 'FontSize', 12);
    ylabel('ERT', 'FontSize', 12 );

    t = title(['Fun=' fun], 'FontSize', 12);
    l = legend(ESfuns(1, :), ESfuns(2, :), ESfuns(3, :)); 
    set(t,'Interpreter','none');    % to block function of underscore _ in string 
    set(l,'Interpreter','none'); 
    
    tmp = strcat('./figures/ERT_', fun, '.png');
    print(tmp, '-dpng');

    figure(figOffset + 1)
    hold off;
    xlabel('N', 'FontSize', 12);
    ylabel('Ps', 'FontSize', 12 );

    t = title(['Fun=' fun], 'FontSize', 12);
    l = legend(ESfuns(1, :), ESfuns(2, :), ESfuns(3, :));
    set(t,'Interpreter','none'); 
    set(l,'Interpreter','none');
    
    tmp = strcat('./figures/ERT_Ps_', fun, '.png');
    print(tmp, '-dpng');
    
    figOffset = figOffset + 1;
end
