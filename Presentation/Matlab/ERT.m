function [ert, ps] = ERT(ESfun, Fitfun, trials, yP, sigmaP, my, rho, lambda, tau, F_target)
    global N;
   
    ert = 0;
    ps = 0;
    
    E_ru = 0;
    E_rs = 0;
    
    nr_Succ = 0;        % number of successfull trials
    nr_Unsucc = 0;      % number of unsuccessfull trials
    nr_FE_Succ = 0;     % number of function evalutions in successfull trials
    nr_FE_Unsucc = 0;   % number of function evalutions in unsuccessfull trials
    
    for k=1:trials
        if(strcmp(ESfun, 'ES_MyMy_Lambda_Sigma'))
         %  [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter] = ES_MyMy_Lambda_Sigma(fun, y_parental, m_strength, m_stop, my, rho, lambda, tau, F_target)
            [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter, success] = feval(ESfun, Fitfun, yP, sigmaP, my, rho, lambda, tau, F_target);
        elseif(strcmp(ESfun, 'CMA_ES'))
            % [generations, y_p, sigma_evolution, F_evolution, sigmaNorm_evolution, funcalls, success] =    CMA_ES(fun, y_p, sigma, my, lambda, F_target)
            [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter, success] = feval(ESfun, Fitfun, yP, sigmaP, my, lambda, F_target);
        elseif(strcmp(ESfun, 'PSO_ConstrictionFactor'))
            % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, ...);
            [g_counter, y_optimal, f_evolution, f_counter, success] = feval(ESfun, Fitfun);
        end
      if(success) 
        nr_Succ = nr_Succ + 1;
        nr_FE_Succ = nr_FE_Succ + f_counter;
      else 
        nr_FE_Unsucc = nr_FE_Unsucc + f_counter; 
      end
      
      msg = strcat(int2str(k), '. trial done');
      disp(msg)
    end
    
    nr_Unsucc = trials - nr_Succ;
    ps = nr_Succ / trials;                % 2.12 slide 52
    
    if(nr_Unsucc ~= 0)
      E_ru = nr_FE_Unsucc / nr_Unsucc;    % 2.13 slide 52
    end
    
    if(nr_Succ == 0)
      disp('No successfull computation -> ERT cannot be calculated!')
      ert = 0;
    else
      E_rs = nr_FE_Succ / nr_Succ;        % 2.13 slide 52
      ert = (1 / ps - 1) * E_ru + E_rs;     % 2.11 slide 51
    end
end