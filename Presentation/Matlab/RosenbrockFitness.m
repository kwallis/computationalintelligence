function [fitness] = RosenbrockFitness(y)
    global N;    
    fitness = 0;

    for i=1:(N - 1)
        fitness = fitness + 100 * (y(i)^2 - y(i+1))^2 + (y(i) - 1)^2;  
    end
  end