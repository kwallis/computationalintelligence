% Authors: Marco Sohm, Kevin Wallis
% input parameters:
% fun:          function name               [string]
% N:            search dimension            [integer value]
% omega:        interior weight             [real value]
% C1:           self confidence             [real value]
% C2:           swarm confidence            [real value]
% Vmax:         max velocity                [real value]
% useLocalNeighborhood:  neighboorhood top  [int=boolean] 0=globalNeighborhood
% F_target      Fitness to be reached(stop) [real value]
% doPlots       display plots?              [int=boolean] 0=no plots
% plotMinFitness plot min or average fitness [[int=boolean] 0=averageFit

% output parameters:
% g_counter:    number of generations     [integer value]
% y_optimal:    optimal vector y          [n x 1] 
% f_evolution:  fitness over generations  [g_counter x 1]     
% f_counter:  	number of function eval   [integer value]
% success:      was F_target reached      [integer value]
function [g_counter, y_optimal, f_evolution, f_counter, success] = PSO_VmaxDynamic(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness)
    global N;    
    global POSITION;
    global VELOCITY;
    global PERSONAL_BEST;
    
    % default parameters
    if ~exist('fun', 'var')
        fun = 'QuadraticSphereFitness';
    end
    if ~exist('N', 'var')
        N = 2;
    end
    if ~exist('omega', 'var')
        omega = 0.9;    % weight factor | ~1 for global optimum
    end
    if ~exist('C1', 'var')
        C1 = 1.494;     % self confidence
    end
    if ~exist('C2', 'var')
        C2 = 1.494;     % swarm confidence | usually C1 = C2 = 1.494
    end
    if ~exist('Vmax', 'var')    % TODO Vmax!??!
        Vmax = 3;       % max velocity | not necessary if constriction factor is used
    end
    if ~exist('swarmSize', 'var')
        swarmSize = 25; % size of swarm | usually btw. 10-60
    end
    if ~exist('F_target', 'var')
        F_target = 1e-3; % target fitness value
    end
    if ~exist('doPlots', 'var')
        doPlots = 0; % target fitness value
    end
    if ~exist('useLocalNeighborhood', 'var')
        useLocalNeighborhood = 0; 
    end
    if ~exist('plotMinFitness', 'var')
        plotMinFitness = 1; 
    end
    % "constants" | do not alter
    POSITION = 1;
    VELOCITY = 2;
    PERSONAL_BEST = 3;
    
    % init
    success = 0;
    g_counter = 0;
    g_max = 10000;
    f_counter = 0;
    f_evolution = NaN * ones(10000, 1);  % init for better performance - ignore NaN values in plot with: plot(f_evolution(~isnan(f_evolution))) 
    g = 0;
    
    % Input: Randomly initialized position and velocity of the particles: Xi(0) and Vi(0)
    % http://de.mathworks.com/help/matlab/math/multidimensional-arrays.html
    % page 1 of multidimensional array = position x | each column represents
    % a single particle | all columns represent the whole swarm
    particles(:, :, POSITION) = 5 * ones(N, swarmSize) + 10 * randn(N, swarmSize);
    % page 2 = velocity v
    particles(:, :, VELOCITY) = Vmax / 5 * randn(N, swarmSize);
    % init p(0) and g(0) with x(0)
    % page 3 = personal best experience | vector p
    particles(:, :, PERSONAL_BEST) = particles(:, :, POSITION);
    
    % init fitnessValues with high value
    fitnessValues = inf * ones(1, swarmSize);
    
    if(useLocalNeighborhood)
        topologicalPopulationArray = zeros(2, swarmSize);
        for s=1:swarmSize
            if(s == 1)
                topologicalPopulationArray(1, s) = swarmSize;
                topologicalPopulationArray(2, s) = s + 1;
            elseif(s == swarmSize)
                topologicalPopulationArray(1, s) = s - 1;
                topologicalPopulationArray(2, s) = 1;
            else
                topologicalPopulationArray(1, s) = s - 1;
                topologicalPopulationArray(2, s) = s + 1;
            end
        end
    end
    
    plotGrid(fun, doPlots);
    points = plotIt(particles, g_counter, [], fun, doPlots);
    pause(0.33);
    
    while(g_counter < g_max)            % While terminating condition is not reached do
        for i=1:swarmSize               % for i = 1 to number of particles
            fitness_i = feval(fun, particles(:, i, POSITION));
            if(fitness_i < fitnessValues(i))
                particles(:, i, PERSONAL_BEST) = particles(:, i, POSITION);     % update best personal experience
                fitnessValues(i) = fitness_i;                                   % save best personal fitness value
            end
            
            g = i;
            
            if(useLocalNeighborhood)
                for j=1:size(topologicalPopulationArray, 1)
                    if(fitnessValues(topologicalPopulationArray(j, i)) < fitnessValues(g))
                        g = topologicalPopulationArray(j, i);
                    end
                end
            else % global best
                g = find(fitnessValues == min(fitnessValues));
                g = g(1, 1);   % if two or more values are identical -> g is an array
            end

            if(fitnessValues(g) < F_target) % terminating condition 
                success = 1;
                break;
            end
            

            velocity_i = omega * particles(:, i, VELOCITY) + C1 * rand(N, 1).* (particles(:, i, PERSONAL_BEST) - particles(:, i, POSITION)) + C2 * rand(N, 1).* (particles(:, g, PERSONAL_BEST) - particles(:, i, POSITION));
            particles(:, i, VELOCITY) = max(-Vmax, min(Vmax, velocity_i)); % max, min ... within range of [-Vmax, +Vmax]
            % Update the position of the particle;
            particles(:, i, POSITION) = particles(:, i, POSITION) + particles(:, i, VELOCITY);
        end
        
        % Vmax adaption
        if(Vmax > 0.7)
            Vmax = Vmax * 0.975;
        end
            
        points = plotIt(particles, g_counter, points, fun, doPlots);
        
        f_counter = f_counter + swarmSize;
        g_counter = g_counter + 1;
        if(plotMinFitness)
            f_evolution(g_counter) =  min(fitnessValues);
        else
            f_evolution(g_counter) = sum(fitnessValues) / swarmSize;
        end
        if(fitnessValues(g) < F_target) % terminating condition 
            break;
        end
    end
    
    % Output: Position of the approximate global optima X*
    y_optimal = particles(:, g, PERSONAL_BEST);
end

function [points] = plotIt(particles, g_counter, points, fun, doPlots)
    global N;
    global POSITION;
    if(doPlots)
        if(N == 2)
            delete(points);
            hold on;

            if(g_counter == 0)
                % AXIS([XMIN XMAX YMIN YMAX ZMIN ZMAX])
                axis([-10 10 -10 10 0 50]);
            elseif(g_counter == 35) 
                az = 0;
                el = 90;
                view(az, el);
                axis([-3 3 -3 3 0 20]);  
            end
            points(size(particles, 2) + 1) =  plot(0, 0, '+', 'Color', 'r');

            for i=1:size(particles, 2)
                point = plot3(particles(1, i, POSITION), particles(2, i, POSITION), feval(fun, particles(:, i, POSITION)), 'marker', 'o', 'MarkerFaceColor', 'r'); 
                points(i) = point;
            end
            drawnow;
            hold off;
            pause(0.33);
        end
    end
end

function [func] = plotGrid(fun, doPlots)    
     if(doPlots)
        figure(10)
        clf reset;
        [x,y] = meshgrid(linspace(-4, 4), linspace(-4, 4));
        if(strcmp(fun, 'QuadraticSphereFitness'))
            % Sphere
            z = x.^2 + y.^2;
        elseif(strcmp(fun, 'SchwefelFitness'))
            % Schwefel
            z = x.^2 + (x + y).^2;
        elseif(strcmp(fun, 'RastriginFitness'))
            % Rastrigin
            z = 20 + (x.^2-10*cos(2*pi*x)) + (y.^2-10*cos(2*pi*y));
        elseif(strcmp(fun, 'RosenbrockFitness'))
            % Rosenbrock 
            z = (100 * (x.^2-y).^2+(x-1).^2);
        end
        func = mesh(x,y,z);
    end
end
