function [totalSum] = SchwefelFitness(y)
    global N;
    totalSum = 0;
    
    for i=1:N
        innerSum = 0;
        for j=1:i
          innerSum = innerSum + y(j);  
        end
        totalSum = totalSum + innerSum^2;
    end
end