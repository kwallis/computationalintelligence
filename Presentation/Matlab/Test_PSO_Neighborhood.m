% Authors: Marco Sohm, Kevin Wallis
clear all;
global N;

more off;
pause off;   % set pause off; if no animation is necessary

%fun = 'QuadraticSphereFitness';
%fun = 'RosenbrockFitness';
%fun = 'SchwefelFitness';
fun = 'RastriginFitness';
N = 5;
useConstrictionFactor = 1;  
if(useConstrictionFactor)
    C1 = 2.05;
    C2 = 2.05;
else
    C1 = 1.494;
    C2 = 1.494;
end
Vmax = 3;
omega = 0.9;
swarmSize = 25;
neighborhoods = [0; 1];  % implemented as social ring topology - social: not based on physical distances btw particles
F_target = 1e-7;            % ring topology: particle 2 has neighboors: 1 & 3, 3 has 2 & 4, 1 has last & 2 ...
 
figure(1)
clf reset;
hold on;
colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];
for i=1:size(neighborhoods)
    useLocalNeighborhood = neighborhoods(i);
    if(useConstrictionFactor)
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness)
        [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, 0, 1);
    else
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
        [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 0);
    end

    disp('generations PSO')
    disp(g_counter)
    disp('func eval PSO')
    disp(f_counter)
    disp('y_optimal PSO')
    disp(y_optimal)
    
    semilogy(f_evolution(~isnan(f_evolution)), '-', 'Color',  colors(i), 'linewidth', 2);
    set(gca,'yscale','log');
    % http://www.mathworks.com/matlabcentral/answers/95921-why-does-the-semilogy-function-not-plot-onto-a-logarithmic-scale-in-matlab-6-5-r13
end

xlabel( 'Generations', 'FontSize', 12);
ylabel( 'Fitness', 'FontSize', 12 );
title('PSO', 'FontSize', 12);
legend('Global', 'Local'); 

hold off;
if(useConstrictionFactor)
    print('./figures/PSO_ConstrictionFactor_Neighborhood','-dpng')
else
    print('./figures/PSO_Neighborhood','-dpng')
end
pause off;