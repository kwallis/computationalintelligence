function [fitness] = RastriginFitness(y)
    global N;
    fitness = 10 * N + sum(y.^2 - 10 * cos(2 * pi * y));  % .^ Element-Wise square
end
