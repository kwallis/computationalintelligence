% Authors: Marco Sohm, Kevin Wallis
clear all;
global N;

more off;
pause off;   % set pause off; if no animation is necessary

dynamics = char('None', 'Vmax', 'Omega');

fun = 'QuadraticSphereFitness';
%fun = 'RosenbrockFitness';
%fun = 'SchwefelFitness';
%fun = 'RastriginFitness';
N = 2;
useConstrictionFactor = 0; % TODO: constriction factor has no dynamic Vmax
% usually constriction factor has no Vmax at all, but it could be used
if(useConstrictionFactor)
    C1 = 2.25;
    C2 = 2.25;
else
    C1 = 1.494;
    C2 = 1.494;
end
omega = 0.9;
Vmax = 3;
swarmSize = 10;
useLocalNeighborhood = 0;  % implemented as social ring topology - social: not based on physical distances btw particles
F_target = 1e-3;            % ring topology: particle 2 has neighboors: 1 & 3, 3 has 2 & 4, 1 has last & 2 ...

% average g_counter over #runs
runs = 250;

colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];

figure(1)
clf reset;
hold on;
for i=1:size(dynamics, 1)
    dynamic = dynamics(i, :); 
    dynamic = deblank(dynamic);
    
    g_counterAverage = 0;
    for k=1:runs
        if(strcmp(dynamic, 'None'))
            if(useConstrictionFactor)
                % [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness)
                [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, 1, 0);
            else
                % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
                [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 0);
            end
        elseif(strcmp(dynamic, 'Omega'))
             % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
            [g_counter, y_optimal, f_evolution, f_counter] = PSO_OmegaDynamic(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 0);
        elseif(strcmp(dynamic, 'Vmax'))
             % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
            [g_counter, y_optimal, f_evolution, f_counter] = PSO_VmaxDynamic(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 0);
        end
        g_counterAverage = g_counterAverage + g_counter;
    end
    
    semilogy(f_evolution(~isnan(f_evolution)), '-', 'Color',  colors(i), 'linewidth', 2);
    
    g_counterAverage = g_counterAverage / runs;
    disp('Dynamic')
    disp(dynamic)
    disp('g_counterAverage PSO')
    disp(g_counterAverage)
end

set(gca,'yscale','log');
% http://www.mathworks.com/matlabcentral/answers/95921-why-does-the-semilogy-function-not-plot-onto-a-logarithmic-scale-in-matlab-6-5-r13

xlabel( 'Generations', 'FontSize', 12);
ylabel( 'Fitness', 'FontSize', 12 );
title('PSO', 'FontSize', 12);
legend('No dynamic', 'Vmax dynamic', 'Omega dynamic'); 

hold off;
if(useConstrictionFactor)
    print('./figures/PSO_ConstrictionFactor_DynamicComparison','-dpng')
else
    print('./figures/PSO_DynamicComparison','-dpng')
end
pause off;