% Authors: Marco Sohm, Kevin Wallis
% input parameters:
% fun:          function name             [string]
% N:            search dimension          [integer value]

% output parameters:
% g_counter:    number of generations     [integer value]
% y_optimal:    optimal vector y          [n x 1] 
% m_evolution:  mutation over generations [n x 1]     
% f_evolution:  fitness values            [n x 1]
% g_counter:    number of function eval   [integer value]
function [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, N, omega, C1, C2, Vmax, swarmSize, F_target)
    global POSITION;
    global VELOCITY;
    global FITNESS;
    global PERSONAL_BEST;
    global NEIGHBORHOOD_BEST;

    % default parameters
    if ~exist('fun', 'var')
        fun = 'QuadraticSphereFitness';
    end
    if ~exist('N', 'var')
        N = 10;
    end
    if ~exist('omega', 'var')
        omega = 0.9;    % weight factor | ~1 for global optimum
    end
    if ~exist('C1', 'var')
        C1 = 1.494;     % self confidence
    end
    if ~exist('C2', 'var')
        C2 = 1.494;     % swarm confidence | usually C1 = C2 = 1.494
    end
    if ~exist('Vmax', 'var')    % TODO Vmax!??!
        Vmax = 3;       % max velocity | not necessary if constriction factor is used
    end
    if ~exist('swarmSize', 'var')
        swarmSize = 10; % size of swarm | usually btw. 20-60
    end
    if ~exist('F_target', 'var')
        F_target = 1e-3; % target fitness value
    end
    % "constants" | do not alter
    POSITION = 1;
    VELOCITY = 2;
    FITNESS = 3;
    PERSONAL_BEST = 4;
    NEIGHBORHOOD_BEST = 5;
    
    % init
    g_counter = 0;
    g_max = 1000;
    f_counter = 0;
    f_evolution = [];   
    indexBestValue = 0;
    
    % other model with constriction factor
    % chi = 0;    % constriction factor
    % omega and Vmax are not needed

    % Input: Randomly initialized position and velocity of the particles: Xi(0) and
    % Vi(0)
    % http://de.mathworks.com/help/matlab/math/multidimensional-arrays.html
    % page 1 of multidimensional array = position x | each column represents
    % a single particle | all columns represent the whole swarm
    particles(:, :, POSITION) = 1 * ones(N, swarmSize) + 10 * randn(N, swarmSize);
    % page 2 = velocity v
    particles(:, :, VELOCITY) = Vmax / 5 * randn(N, swarmSize);
    % page 3 = fitness | only row 1 is used for the real value fitness
    % init with high value
    particles(1, :, FITNESS) = inf * ones(1, swarmSize);
    % init p(0) and g(0) with x(0)
    % page 4 = personal best experience | vector p
    particles(:, :, PERSONAL_BEST) = particles(:, :, POSITION);
    % page 5 = neighborhood best experience | vector g | 
    % if N(P) = S  g = globally best experience, g is the same vector for
    % all particles in that case
    particles(:, :, NEIGHBORHOOD_BEST) = particles(:, :, POSITION);
        
    figure(10)
    plotIt(particles, g_counter);
    pause(0.33);
    
    while(g_counter < g_max)        % While terminating condition is not reached do
       for i=1:swarmSize            % for i = 1 to number of particles

            fitness_i = feval(fun, particles(:, i, POSITION));     % Evaluate the fitness:= f(Xi);
            
            % Update pi | personal best experience
            if fitness_i < particles(1, i, FITNESS)
                particles(:, i, PERSONAL_BEST) = particles(:, i, POSITION);    % update best personal experience
                particles(1, i, FITNESS) = fitness_i;   % page 3 = best fitness of particle i 
            end
       end
       % find global best particle | TODO - adapt for other neighborhoods
       indexBestValue = find(particles(1, :, FITNESS) == min(particles(1, :, FITNESS)));
       indexBestValue = indexBestValue(1, 1);   % if two or more values are identical -> indexBestValue is an array
       if(particles(1, indexBestValue, FITNESS) < F_target) % terminating condition 
            break;
       end
       
       % Update global best experience | TODO - adapt for other neighborhoods
       for k=1:swarmSize
            particles(:, k, NEIGHBORHOOD_BEST) = particles(:, indexBestValue, PERSONAL_BEST);
       end
       
       for j=1:swarmSize
            velocity_j = omega * particles(:, j, VELOCITY) + C1 * rand(N, 1).* (particles(:, j, PERSONAL_BEST) - particles(:, j, POSITION)) + C2 * rand(N, 1).* (particles(:, j, NEIGHBORHOOD_BEST) - particles(:, j, POSITION));
            particles(:, j, VELOCITY) = max(-Vmax, min(Vmax, velocity_j)); % max, min ... within range of [-Vmax, +Vmax]
            % Update the position of the particle;
            particles(:, j, POSITION) = particles(:, j, POSITION) + particles(:, j, VELOCITY);
       end
       plotIt(particles, g_counter);
       f_evolution =  [f_evolution ; min(particles(1, :, FITNESS))];
       f_counter = f_counter + swarmSize;
       g_counter = g_counter + 1;
    end
    
    % Output: Position of the approximate global optima X*
    y_optimal = particles(:, indexBestValue, PERSONAL_BEST);
end

% old version
% for n=1:N
   % Adapt velocity of the particle using equations (1);
   % particles(n, j, VELOCITY) = omega * particles(n, j, VELOCITY)  ...
   %     + C1 * rand() * (particles(n, j, PERSONAL_BEST) - particles(n, j, POSITION)) ...
   %     + C2 * rand() * (particles(n, j, NEIGHBORHOOD_BEST) - particles(n, j, POSITION));
   % Update the position of the particle;
 % particles(n, j, POSITION) = particles(n, j, POSITION) + particles(n, j, VELOCITY);
% end

function plotIt(particles, g_counter)
    global N;
    global POSITION;
    
    if(N == 2)
        clf reset;
        hold on;
        if(g_counter < 35)
            axis([-10 110 -10 110]);
        else 
            axis([-10 10 -10 10]);  
        end
        plot(0, 0, '+', 'Color', 'r');
        plot(particles(1, :, POSITION), particles(2, :, POSITION), '*');  
        drawnow;
        hold off;
        pause(0.33);
    end
end
