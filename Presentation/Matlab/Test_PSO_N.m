% Authors: Marco Sohm, Kevin Wallis
clear all;
global N;

more off;
pause on;   % set pause off; if no animation is necessary

fun = 'QuadraticSphereFitness';
%fun = 'RosenbrockFitness';
%fun = 'SchwefelFitness';
%fun = 'RastriginFitness';
Ns = [4; 8; 16; 32; 64];
useConstrictionFactor = 0;
if(useConstrictionFactor)
    C1 = 2.25;
    C2 = 2.25;
else
    C1 = 1.494;
    C2 = 1.494;
end
omega = 0.7;
Vmax = 3;
swarmSize = 40;
useLocalNeighborhood = 0;  % implemented as social ring topology - social: not based on physical distances btw particles
F_target = 1e-3;            % ring topology: particle 2 has neighboors: 1 & 3, 3 has 2 & 4, 1 has last & 2 ...


figure(1)
clf reset;
hold on;
for s=1:size(Ns)
    N = Ns(s);

    colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];
        
    if(useConstrictionFactor)
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness)
        [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, 0, 1);
    else
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
        [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 0, 1);
    end


    disp('generations PSO')
    disp(g_counter)
    disp('func eval PSO')
    disp(f_counter)

    semilogy(f_evolution(~isnan(f_evolution)), '-', 'Color',  colors(s), 'linewidth', 2);
    set(gca,'yscale','log');
    % http://www.mathworks.com/matlabcentral/answers/95921-why-does-the-semilogy-function-not-plot-onto-a-logarithmic-scale-in-matlab-6-5-r13
end 
hold off;
xlabel( 'Generations', 'FontSize', 12);
ylabel( 'Fitness', 'FontSize', 12 );
title('PSO', 'FontSize', 12);
legend(['N=' int2str(Ns(1))], ['N=' int2str(Ns(2))],['N=' int2str(Ns(3))],['N=' int2str(Ns(4))],['N=' int2str(Ns(5))]); 

if(useConstrictionFactor)
    print('./figures/PSO_ConstrictionFactor_N_Variation','-dpng')
else
    print('./figures/PSO_N_Variation','-dpng')
end
pause off;