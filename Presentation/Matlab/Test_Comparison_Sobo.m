% Authors: Marco Sohm, Kevin Wallis
clear all;
global N;
global fun_counter;
more off;
randn('state', 7);

colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];

% PSO
useConstrictionFactor = 0;  
if(useConstrictionFactor)
    C1 = 2.05;
    C2 = 2.05;
else
    C1 = 1.494;
    C2 = 1.494;
end
N = 2;
omega = 0.7;
Vmax = 3;
swarmSize = 10;
useLocalNeighborhood = 0;  % implemented as social ring topology - social: not based on physical distances btw particles
F_target = 1e-3;

% other ES    
doSigma = 0;
doSigmaNorm = 0;  

% same init as PSO.m: particles(:, :, POSITION) = 5 * ones(N, swarmSize) + 10 * randn(N, swarmSize)
yP = 5 * ones(1, N) + 10 * randn(1, N);
sigmaP = 1;
if(doSigmaNorm)
   sigmaP = 0.001;
end
my=3;
lambda = 10;
sigma_stop = 10^(-5);
rho = 3;
tau = 1 / sqrt(2 * N);

c = 1/sqrt(N);
%c = 4/(N + 4);
d = sqrt(N);
%d = 1/c + 1;
c_v = 1/sqrt(N);
%c_v = 4/(N + 4);
c_c = 2/(N + sqrt(2))^2;

funsData = ['QuadraticSphereFitness'; 'SchwefelFitness       '; 'RastriginFitness      '; 'RosenbrockFitness     '];
funs = cellstr(funsData);

for i=1: size(funs, 1)

    fun = funs{i}; 
    fun = deblank(fun);
    disp('fun')
    disp(fun)
  
    if(useConstrictionFactor)
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness)
        [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, 0, 1);
    else
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
        [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 0, 0);
    end
    
    figure(i)
    clf reset;
    hold on;
    
    disp('generations PSO')
    disp(g_counter)
    disp('func eval PSO')
    disp(f_counter)

    semilogy(f_evolution(~isnan(f_evolution)), '-', 'Color',  colors(1), 'linewidth', 2);
    % http://www.mathworks.com/matlabcentral/answers/95921-why-does-the-semilogy-function-not-plot-onto-a-logarithmic-scale-in-matlab-6-5-r13

    % CMA-ES
    fun_counter = 0;
    [g_counter_CMA_ES, y_optimal_CMA_ES, hist_f_CMA_ES, hist_sigma_CMA_ES, hist_sigma_norm_CMA_ES] = ...
        ES_MuMuI_Komma_Lambda_CMA(fun, yP, sigmaP, sigma_stop, my, lambda, c, d, c_v, c_c, F_target);

    disp('CMA-ES');
    disp(['  Generation counter: ' num2str(g_counter_CMA_ES)]);
    disp(['  Function calls: ' num2str(fun_counter)]);
    fun_counter_CSA_ES = fun_counter;

    semilogy(hist_f_CMA_ES, '-', 'Color', colors(2), 'linewidth', 2);
    
    % comparison with ES_MyMy_Lambda_Sigma
    % sigmaP-SA-ES
    fun_counter = 0;
    [g_counter_SA_ES, y_optimal_SA_ES, hist_f_SA_ES, hist_sigma_SA_ES, hist_sigma_norm_SA_ES] = ...
        ES_MuMuI_Lambda_Sigma(fun, yP, sigmaP, sigma_stop, my, rho, lambda, tau, F_target);

    disp('SA-ES');
    disp(['  Generation counter: ' num2str(g_counter_SA_ES)]);
    disp(['  Function calls: ' num2str(fun_counter)]);
    fun_counter_SA_ES = fun_counter;

    semilogy(hist_f_SA_ES, '-', 'Color', colors(3), 'linewidth', 2);

    xlabel('Generations', 'FontSize', 12);
    ylabel('Fitness', 'FontSize', 12);

    set(gca,'yscale','log');
    title(['PSO, CMA-ES and \sigmaSA-ES comparision; N=' int2str(N) '; fun=' fun], 'FontSize', 12);
    legend('PSO', '(3/3_I, 10)-CMA-ES', '(3/3_)I, 10)-\sigmaSA-ES'); 

    hold off;

    tmp = strcat('./figures/Comparison_Fitness', fun, '.png');
    print(tmp, '-dpng');
    disp('-------------------------------------------------')
end