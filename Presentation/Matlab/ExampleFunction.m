pause on;

%[x,y] = meshgrid(-3:1:3, -3:1:3);
[x,y] = meshgrid(linspace(-3,3), linspace(-3,3));

% Sphere
% z = x.^2 + y.^2;
% Schwefel
% z = x.^2 + (x + y).^2;
% Rastrigin
% z = 20 + (x.^2-10*cos(2*pi*x)) + (y.^2-10*cos(2*pi*y));
% Rosenbrock 
z = (100 * (x.^2-y).^2+(x-1).^2);

figure(1)
hold on;
% set(gca,'zlim',[0 1000]);

func = mesh(x,y,z);

%alpha(func, 0);
point = plot3(0,0,0,'marker','o', 'MarkerFaceColor', 'k');

for i=1:20
    delete(point);
    point = plot3(0,0,i,'marker','o', 'MarkerFaceColor', 'k');
    drawnow;
    pause(1);
end

hold off;

%z = x.*sin(4*y.*pi) - y.*sin(4*x.*pi+pi)+1;
%figure(2)
%mesh(x,y,z);

%z = cos(x.*y);
%figure(3)
%mesh(x,y,z);


%figure(4)
%sphere(50);

%figure(5)
%surf(x,y,z);