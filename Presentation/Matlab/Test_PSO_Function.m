% Authors: Marco Sohm, Kevin Wallis
clear all;
global N;

more off;
pause off;   % set pause off; if no animation is necessary

fun = 'QuadraticSphereFitness';
%fun = 'RosenbrockFitness';
%fun = 'SchwefelFitness';
%fun = 'RastriginFitness';
N = 2;
useConstrictionFactor = 0;
if(useConstrictionFactor)
    C1 = 2.05;
    C2 = 2.05;
else
    C1 = 1.494;
    C2 = 1.494;
end
omega = 0.6;
Vmax = 3;
swarmSize = 10;
useLocalNeighborhood = 0;  % implemented as social ring topology - social: not based on physical distances btw particles
F_target = 1e-7;            % ring topology: particle 2 has neighboors: 1 & 3, 3 has 2 & 4, 1 has last & 2 ...
plotMinFitness = 1;

% average g_counter over #runs
runs = 1;
g_counterAverage = 0;

colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];
for k=1:runs
    if(useConstrictionFactor)
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness)
        [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, 1, plotMinFitness);
    else
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
        [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 1, plotMinFitness);
    end
    g_counterAverage = g_counterAverage + g_counter;
end

g_counterAverage = g_counterAverage / runs;
disp('g_counterAverage PSO')
disp(g_counterAverage)

disp('generations PSO')
disp(g_counter)
disp('func eval PSO')
disp(f_counter)

figure(1)
clf reset;
hold on;
semilogy(f_evolution(~isnan(f_evolution)), '-', 'Color',  colors(1), 'linewidth', 2);
set(gca,'yscale','log');
% http://www.mathworks.com/matlabcentral/answers/95921-why-does-the-semilogy-function-not-plot-onto-a-logarithmic-scale-in-matlab-6-5-r13

xlabel( 'Generations', 'FontSize', 12);
ylabel( 'Fitness', 'FontSize', 12 );
title('PSO', 'FontSize', 12);
legend('PSO'); 

hold off;
if(useConstrictionFactor)
    print('./figures/PSO_ConstrictionFactor','-dpng')
else
    print('./figures/PSO','-dpng')
end
pause off;