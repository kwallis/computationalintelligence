% function ES_MuMuI_Lambda_Sigma
% 1.mu = mu: size of parent population
% 2.mu = rho: size of family (parents) : 1 <= 2.mu <= 1.mu
% Comma Selection
% Authors: Nicolaj H�ss, Martin Sobotka, Marco Sohm, Kevin Wallis

% input parameters:
% fun:          function name             [string]
% y_parental:   initial parent vector     [1 x N] 
% m_strength:   mutation strength         [real value]
% m_stop:       mutation termination      [integer value]
% N:            search dimension          [integer value]
% lambda:       lambda value              [integer value]
% tau:          tau value                 [real value]

% output parameters:
% hist_sigma:  mutation over generations [N x 1]     
% g_counter:    number of generations    [integer value]
% hist_f:  fitness values                [N x 1]
% y_optimal:    optimal vector y         [1 x N]

function [g_counter, y_optimal, hist_f, hist_sigma, hist_sigma_norm] = ES_MuMuI_Lambda_Sigma(fun, y_parental, m_strength, m_stop, mu, rho, lambda, tau, F_target)
  global N;   
  % initialize
  hist_f = [];
  hist_sigma = [];
  hist_sigma_norm = [];
  y_optimal = [];
  g_counter = 0;
  
  p_population = [];
  % p_population consists of ym sm F(ym)  where ym=y_parental sm=m_strength F(ym)=fitness
  % p_population = [mu x N+2] matrix
  for i=1:mu
	  p_population = [p_population ; y_parental m_strength feval(fun, y_parental)];   % p_population = Bmu
  end
  
  % (first generation) for the first loop step - originally placed inside the loop after vector initialization
  family = Marriage(p_population, rho);
  m_srenght_recomb = M_Strenght_Recombination_Average(family, rho, N);  % m_srenght_recomb = sRl
  
  % mutation over generations for the first data
  hist_sigma = [hist_sigma ; m_srenght_recomb];
  hist_sigma_norm = [hist_sigma_norm ; (m_srenght_recomb/norm(y_parental) * N)];
  % fitness over generations, average
  hist_f =  [hist_f ; feval(fun, y_parental)];
  
  while g_counter < 1000
    m_lambda = [];
    y_lambda = [];
    f_lambda = [];
    B_lambda = [];
    al = [];
    
    % do this outside of the lambda loop because its the same for all in mumuI algorithm
    
    % select rho parents(family) from parent population p_population 
    % in mumuI select all parents
    %family = Marriage(p_population, rho);
    % recombination mutation strength 
    %m_srenght_recomb = M_Strenght_Recombination_Average(family, rho, N);  % m_srenght_recomb = sRl
    % recombination object parameters 
    y_recomb = Y_Recombination_Average(family, rho, N);                   % y_recomb = yRl
    
    % mutation over generations for all the other data
    hist_sigma = [hist_sigma ; m_srenght_recomb];
	hist_sigma_norm = [hist_sigma_norm ; (m_srenght_recomb/norm(y_recomb) * N)];
    % fitness over generations, average
    hist_f =  [hist_f ; feval(fun, y_recomb)];
	
    for l=1:lambda
      xi = exp(tau * randn());
	  % mutate strategy parameter = mutation strength
      m_lambda = [m_lambda ; xi * m_srenght_recomb];                      % m_lambda = sl
      % mutate object parameter
      y_lambda = [y_lambda ; RealOffspring(y_recomb, m_lambda(l))];       % y_lambda = yl
      % calc fitness
      f_lambda = [f_lambda ; feval(fun, y_lambda(l, :))];                 % f_lambda = Fl
      % individual consists of object param, sigma, fitness
      al = [y_lambda(l, :) m_lambda(l) f_lambda(l)];
      B_lambda = [B_lambda ; al];     	  
    end
    % select mu best offsprings of B_lambda = p_population(B_mu):
    p_population = [];
    
    for i=1:mu
      indexBestValue = find(f_lambda == min(f_lambda));
    
      Bmu_m_strength = m_lambda(indexBestValue);
      Bmu_fitness = f_lambda(indexBestValue);
      Bmu_y = y_lambda(indexBestValue, :);
     
      p_population = [p_population ; Bmu_y Bmu_m_strength Bmu_fitness];   % p_population = Bmu
      
      f_lambda(indexBestValue)=inf;    
      % set the current best f_lambda value to inf get the second best f_lambda value in the next iteration
      % f_lambda can be changed because its not needed anymore
    end
  
    g_counter = g_counter + 1;
	% new generation; this two lines were originally placed inside the loop after vector initialization
	family = Marriage(p_population, rho);
    m_srenght_recomb = M_Strenght_Recombination_Average(family, rho, N);  % m_srenght_recomb = sRl
    
    if(hist_f(end) < F_target) % terminating condition 
        break;
    end
  end
  
  y_optimal = Y_Recombination_Average(p_population, rho, N);
  % disp(['y_optimal SA-ES: ',num2str(feval(fun, y_optimal))]);
    end

% Marriage function
% See slide 99
% only for extensions, we only need rho == mu
function [family] = Marriage(parent_pool, rho)
  family = parent_pool;
  %mu = size(parent_pool, 2);
	%if (rho < mu)
		% TODO (if needed) pick randomly rho parents from the pool and return it
	%else if (rho == mu) % mumuI
	%	family = parent_pool
	%else
		% rho > mu
		% Illegal case
	%end
end


% function to create offsprings
function [y_offspring] = RealOffspring(y_parental, m_strength)
    mutation = randn(size(y_parental,2),1);
    y_offspring = y_parental + m_strength*(mutation');
end

% recombination functions
function [avg] = Y_Recombination_Average(family, rho, N)
  tmp = family;
  tmp(:,[N+1,N+2]) = [];    % remove last two rows of family matrix to get rid of m_strenght and fitness
  avg = Average(tmp, rho);  % tmp is y_parental matrix
end

function [avg] = M_Strenght_Recombination_Average(family, rho, N)
  avg = Average(family(:, N+1), rho);  % family(:, N+1) select all rows and the second column of family matrix
                                       % in column N+1 is m_strength=sm, transpone it to get column vector
end

function [avg] = Average(family, rho)
  [K, M] = size(family);   
  avg = zeros(1, M);
  
  % calculate arithmetic mean
  for i = 1:M   
      avg(i) = sum(family(:,i)) / rho;
  end
end