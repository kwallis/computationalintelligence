%%%

function [g, y, hist_f, hist_sigma, hist_sigma_norm] = ...
	ES_MuMuI_Komma_Lambda_CMA(fitness_fun, y, sigma, sigma_stop, mu, lambda, c, d, c_v, c_c, F_target)
	global N;   
	% init
	y_parent = y';
	g = 0;
    g_stop = 1000;
	hist_f = [];
	hist_sigma = [];
	hist_sigma_norm = [];
	s = zeros(N, 1);
	v = zeros(N, 1);
	offspring = [];
	Covariance = eye(N);
	
	% fetch history data
	hist_f(1) = feval(fitness_fun, y_parent');
	hist_sigma(1) = sigma;
	hist_sigma_norm(1) = (sigma / norm(y_parent)) * N;
	
	while (g < g_stop)
		
		% Create offsprings
		M = chol(Covariance)';
		for l = 1:lambda
			offspring(l).N = randn(N, 1);
			offspring(l).w = M * offspring(l).N;
			offspring(l).y = y_parent + sigma * offspring(l).w;
			offspring(l).f = feval(fitness_fun, offspring(l).y');
		end
		
		% sort offspring by fitness value
		[~, order] = sort([offspring(:).f], 'ascend');
		sortedOffspring = offspring(order);
		
		% calculate recombinat
		n_recomb = 1/mu * sum([sortedOffspring(1:mu).N]')';
		w_recomb = 1/mu * sum([sortedOffspring(1:mu).w]')';
		
		y_parent = y_parent + sigma*w_recomb;
		
		v = (1 - c_v)*v + sqrt(mu * c_v * (2 - c_v)) * w_recomb;
		
		Covariance = (1 - c_c)*Covariance + c_c * v * v';
		%Covariance = (Covariance + Covariance')/2;
		
		s = (1 - c)*s + sqrt(mu * c * (2 - c)) * n_recomb;

		sigma = sigma * exp((s'*s - N) / (2 * N * d));
		
		g = g + 1;
		
		% fetch history data
		hist_f(g + 1) = feval(fitness_fun, y_parent');
		hist_sigma(g + 1) = sigma;
		hist_sigma_norm(g + 1) = (sigma / norm(y_parent)) * N;
        
        if(hist_f(end) < F_target) % terminating condition 
            break;
        end
	end
	
	y = y_parent;
end
