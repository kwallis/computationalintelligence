% Authors: Marco Sohm, Kevin Wallis
clear all;
global N;

more off;
pause off;   % set pause off; if no animation is necessary

fun = 'QuadraticSphereFitness';
%fun = 'RosenbrockFitness';
%fun = 'SchwefelFitness';
%fun = 'RastriginFitness';
N = 20;
useConstrictionFactor = 0; 
if(useConstrictionFactor)
    Cs = [2.05 2.05; 2.00 2.15; 2.05 2.15; 2.15 2.05];
else
    Cs = [1.494 1.494; 0 1.5; 1.5 0; 2 2];
end
omega = 0.7;
Vmax = 3;
swarmSize = 10;
useLocalNeighborhood = 0;  % implemented as social ring topology - social: not based on physical distances btw particles
F_target = 1e-7;            % ring topology: particle 2 has neighboors: 1 & 3, 3 has 2 & 4, 1 has last & 2 ...


colors = ['k'; 'r'; 'b'; 'm'; 'c'; 'g'];

figure(1)
clf reset;
hold on;
for s=1:size(Cs)
    C1 = Cs(s, 1);
    C2 = Cs(s, 2);
    
    if(useConstrictionFactor)
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness)
        [g_counter, y_optimal, f_evolution, f_counter] = PSO_ConstrictionFactor(fun, C1, C2, swarmSize, useLocalNeighborhood, F_target, 0);
    else
        % [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, doPlots, plotMinFitness);
        [g_counter, y_optimal, f_evolution, f_counter] = PSO(fun, omega, C1, C2, Vmax, swarmSize, useLocalNeighborhood, F_target, 0);
    end
    disp('generations PSO')
    disp(g_counter)
    disp('func eval PSO')
    disp(f_counter)

    semilogy(f_evolution(~isnan(f_evolution)), '-', 'Color',  colors(s), 'linewidth', 2);
    set(gca,'yscale','log');
    % http://www.mathworks.com/matlabcentral/answers/95921-why-does-the-semilogy-function-not-plot-onto-a-logarithmic-scale-in-matlab-6-5-r13

end
hold off;

xlabel( 'Generations', 'FontSize', 12);
ylabel( 'Fitness', 'FontSize', 12 );
title('PSO', 'FontSize', 12);
legend(['C1=' num2str(Cs(1, 1)) '; C2=' num2str(Cs(1, 2))], ['C1=' num2str(Cs(2, 1)) '; C2=' num2str(Cs(2, 2))], ['C1=' num2str(Cs(3, 1)) '; C2=' num2str(Cs(3, 2))], ['C1=' num2str(Cs(4, 1)) '; C2=' num2str(Cs(4, 2))]); 


if(useConstrictionFactor)
    print('./figures/PSO_ConstrictionFactor_C1C2','-dpng')
else
    print('./figures/PSO_C1C2','-dpng')
end
pause off;