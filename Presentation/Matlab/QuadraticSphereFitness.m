function [fitness] = QuadraticSphereFitness(y)
    % if row vector -transform to column vector    
    if(size(y, 1) == 1)
        y = y';
    end

    fitness = abs(y' * y);
end