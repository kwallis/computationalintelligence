% Test of CSA_ES and CMA_ES
% Authors: Marco Sohm, Kevin Wallis

% Init arguments
graphics_toolkit gnuplot
more off;
randn("state", 7);

for f=1:2
  figure(f);
  clf reset;
  hold on;
endfor

fun = 'QuadraticSphereFitness';
N = 40;
yP = ones(1, N);
sigmaP = 1;
my=3;
lambda = 10;
sigma_stop = 10^(-5);

colors = ["k"; "r"; "b"; "m"; "c"; "g"];
%[generations, y_opt, sigma_evolution, F_evolution, sigmaNorm_evolution, funcalls] = CSA_ES(yP, sigma, sigma_stop, N, lambda, my, fun)
[gen_counter, y_opt, sigma_evol, F_evol, sigmaNorm_evolution, funcalls] = CSA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

disp('generations CSA_ES')
disp(gen_counter)
disp('func eval CSA_ES')
disp(funcalls)
figure(1);
semilogy(sigma_evol, '-', 'Color',  colors(1), 'linewidth', 2);
figure(2);
semilogy(F_evol, '-', 'Color',  colors(1), 'linewidth', 2);

% comparison with CMA_ES
[gen_counter, y_opt, sigma_evol, F_evol, sigmaNorm_evolution, funcalls] = CMA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

disp('generations CMA_ES')
disp(gen_counter)
disp('func eval of CMA_ES')
disp(funcalls)
figure(1);
semilogy(sigma_evol, '-', 'Color',  colors(5), 'linewidth', 2);
figure(2);
semilogy(F_evol, '-', 'Color',  colors(5), 'linewidth', 2);

for f=1:2
  figure(f);
  xlabel('Generations', 'FontSize', 12);
endfor

figure(1);
ylabel( 'Mutation', 'FontSize', 12 );
figure(2);
ylabel( 'Fitness', 'FontSize', 12 );

for f=1:2
  figure(f);
  title('(3/3_I, 10)-CSA-ES and (3/3_I, 10)-CMA-ES comparision; N=40', 'FontSize', 12);
  legend('(3/3_I, 10)-CSA-ES', '(3/3_I, 10)-CMA-ES'); 
  hold off;
  if(f == 1)
    print -color -dpng './figures/Task5Sigma.png'
  else
    print -color -dpng './figures/Task5.png'
  endif
endfor