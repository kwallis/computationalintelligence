% Test of CSA_ES, CMA_ES and ES_MyMy_Lambda_Sigma
% Authors: Marco Sohm, Kevin Wallis

% Init arguments
graphics_toolkit gnuplot
more off;
randn("state", 7);
 
doSigma = 0;

N = 30;
yP = ones(1, N);
sigmaP = 1;
my=3;
lambda = 12;
sigma_stop = 10^(-5);

% par.ridge and sharpride dont work
funs = ['CigarFitness'; 'TabletFitness'; 'Ellipsoid2Fitness'; 'ParabolicRidgeFitness'; 'SharpRidgeFitness'; 'DifferentPowersFitness'];
% https://www.gnu.org/software/octave/doc/interpreter/Cell-Arrays-of-Strings.html
funs = ['CigarFitness'; 'TabletFitness'; 'Ellipsoid2Fitness'; 'DifferentPowersFitness'];
colors = ["k"; "r"; "b"; "m"; "c"; "g"];
for i=1: size(funs, 1)
  figure(i)
  clf reset;
  hold on;

  fun = funs(i, :); 
  fun = deblank(fun);
  disp('fun')
  disp(fun)
                                                                            %CMA_ES(y_p, sigma, sigma_stop, lambda, my, fun)
  [g_counter, y_opt, m_evolution, f_evolution, mNorm_evolution, f_counter] = CMA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);


  disp('generations CMA_ES')
  disp(g_counter)
  disp('func eval of CMA_ES')
  disp(f_counter)
  
  if(doSigma)
    semilogy(m_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
  else
    semilogy(f_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
  endif
  
  % comparison with CMA_ES_RankMy
  [g_counter, y_opt, m_evolution, f_evolution, mNorm_evolution, f_counter] = CMA_ES_RankMy(yP, sigmaP, sigma_stop, lambda, my, fun);

  disp('generations CMA_ES_RankMy')
  disp(g_counter)
  disp('func eval of CMA_ES_RankMy')
  disp(f_counter)
  
  if(doSigma)
    semilogy(m_evolution, '-', 'Color',  colors(1), 'linewidth', 2);
  else
    semilogy(f_evolution, '-', 'Color',  colors(1), 'linewidth', 2);
  endif

  % comparison with CMSA_ES
  [g_counter, m_evolution, f_evolution] = CMSA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

  disp('generations ES CMSA_ES')
  disp(g_counter)
  
  if(doSigma)
    semilogy(m_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
  else
    semilogy(f_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
  endif

  xlabel('Generations', 'FontSize', 12);
  if(doSigma)
    ylabel('Mutation', 'FontSize', 12);
  else
    ylabel('Fitness', 'FontSize', 12);
  endif
  title(['CMA-ES, CMA-ES-RankMy and CMSA-ES comparision; N=30; fun=' fun], 'FontSize', 12);
  legend('(3/3_I, 12)-CMA-ES', '(3/3_I, 12)-CMA-ES-RankMy', '(3/3_I, 12)-CMSA-ES'); 

  hold off;
  
  if(doSigma)
    tmp = strcat('./figures/Task9Sigma_', fun, '.png');
  else
    tmp = strcat('./figures/Task9_', fun, '.png');
  endif
  print(tmp, '-dpng');
  disp('-------------------------------------------------')
endfor
