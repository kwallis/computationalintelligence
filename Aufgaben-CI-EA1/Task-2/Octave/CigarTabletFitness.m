function [fitness] = CigarTabletFitness(y)
  y_1 = y(1)^2;
  y_2 = 0;
  N = size(y, 2);
  for i=2:N-1
    y_2 = y_2 + (y(i)^2);
  endfor
  fitness = y_1 + 1e4 * y_2 + 1e8 * (y(N)^2);
endfunction