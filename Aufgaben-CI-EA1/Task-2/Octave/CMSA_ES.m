%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Matlab code of a simple Evolution Strategy applied to the optical lens
% optimization. Stategy type: (mu/mu_I, lambda)-CMSA-ES
% Scalarized minimization of quadratic focal point deviation and lens mass
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Copyright by Hans-Georg Beyer (HGB), 25.06.10. For non-commercial use
%% only. Commercial use requires written permission by Hans-Georg Beyer
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Here starts the CMSA-ES (cf. Pseudocode)
% initialization
function [g_counter, m_evolution, f_evolution] = CMSA_ES(yP, sigmaP, sigma_stop, lambda, my, fun)
  n = size(yP, 2);
  mu = my;
  x = yP';
  sigma = sigmaP; 
  sigma_stop = sigma_stop;
  C = eye(n); 
  tau = 1/sqrt(2*n); tau_c = 1 + n*(n+1)/(2*mu);
  
  g_counter = 0;
  g_max = 10000;
  m_evolution = [sigma];
  f_evolution = [];
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % here starts generation loop
  while( sigma > sigma_stop || g_counter > g_max) % (L2)
    for l=1:lambda
      sigmaTilde(l) = sigma * exp(tau*randn()); % (L3)
      sTilde(:, l) = (det(chol(C))^(-1/n)*chol(C)')*randn(n,1); % (L4)
      xTilde(:, l) = x + sigmaTilde(l)*sTilde(:, l); % (L5)
      fTilde(l) = feval(fun, (xTilde(:, l))'); % (L6)
    end
    [fsorted, r] = sort(fTilde, 'ascend'); % (L7)
    x = 1/mu * sum(xTilde(:, r(1:mu)), 2); % (L8)
    sigma = 1/mu * sum(sigmaTilde(r(1:mu))); % (L9)
    SumSS = zeros(n, n); % (L10)
    for m=1:mu; SumSS = SumSS + sTilde(:, r(m))*sTilde(:, r(m))'; end; % (L10)
    C = (1-1/tau_c)*C + (1/tau_c) * (1/mu)*SumSS; % (L10)
    C = 0.5*(C+C'); % ensure symmetry of C-matrix
    	
    % mutation over generations
    m_evolution = [m_evolution; sigma];
    % fitness over generations, average
    f_evolution =  [f_evolution; mean(fsorted(1:mu))];
    g_counter++;
  end
endfunction

