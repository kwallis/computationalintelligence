% Test of CMA_ES
% Authors: Marco Sohm, Kevin Wallis

% Init arguments
graphics_toolkit gnuplot
more off;
randn("state", 7);
 
doMax = 0;
doMin = 0;
doCondition = 0;

N = 30;
yP = ones(1, N);
sigmaP = 1;
my=3;
lambda = 10;
sigma_stop = 10^(-5);

% change legend accordingly
% par.ridge and sharpride dont work
funs = ['CigarFitness'; 'TabletFitness'; 'Ellipsoid2Fitness'; 'ParabolicRidgeFitness'; 'SharpRidgeFitness'; 'DifferentPowersFitness'];
funs = ['CigarFitness'; 'TabletFitness'; 'Ellipsoid2Fitness'; 'DifferentPowersFitness'];
% https://www.gnu.org/software/octave/doc/interpreter/Cell-Arrays-of-Strings.html

colors = ["k"; "r"; "b"; "m"; "c"; "g"];

figure(1)
clf reset;
hold on;

for i=1: size(funs, 1)

  fun = funs(i, :); 
  fun = deblank(fun);                                                                                                                             
  disp('fun')
  disp(fun)
                                                                                                                                                  % CMA_ES_T7(y_p, sigma, sigma_stop, lambda, my, fun)
  [g_counter, y_opt, m_evolution, f_evolution, mNorm_evolution, f_counter, minEigenvalue_evolution, maxEigenvalue_evolution, condition_evolution] = CMA_ES_T7(yP, sigmaP, sigma_stop, lambda, my, fun);

  disp('generations CMA_ES')
  disp(g_counter)
  disp('func eval of CMA_ES')
  disp(f_counter)
  
  if(doMax)
    semilogy(maxEigenvalue_evolution, '-', 'Color',  colors(i), 'linewidth', 2);
  elseif(doMin)
    semilogy(minEigenvalue_evolution, '-', 'Color',  colors(i), 'linewidth', 2);
  elseif(doCondition)
    semilogy(condition_evolution, '-', 'Color',  colors(i), 'linewidth', 2);
  else
    semilogy(m_evolution, '-', 'Color',  colors(i), 'linewidth', 2);
  endif
endfor

xlabel('Generations', 'FontSize', 12);

if(doMax)
  ylabel('Max eigenvalue', 'FontSize', 12);
elseif(doMin)
  ylabel('Min eigenvalue', 'FontSize', 12);
elseif(doCondition)
  ylabel('Conditional number K', 'FontSize', 12);
else
  ylabel('Mutation', 'FontSize', 12);
endif
title('CMA-ES; N=30', 'FontSize', 12);
% legend('CigarFitness', 'TabletFitness', 'Ellipsoid2Fitness','ParabolicRidgeFitness', 'SharpRidgeFitness', 'DifferentPowersFitness'); 
legend('CigarFitness', 'TabletFitness', 'Ellipsoid2Fitness', 'DifferentPowersFitness'); 

hold off;

if(doMax)
  tmp = strcat('./figures/Task7_MaxEigenvalue', '.png');
elseif(doMin)
  tmp = strcat('./figures/Task7_MinEigenvalue', '.png');
elseif(doCondition)
  tmp = strcat('./figures/Task7_Conditional', '.png');
else
  tmp = strcat('./figures/Task7_Sigma', '.png');
endif
print(tmp, '-dpng');