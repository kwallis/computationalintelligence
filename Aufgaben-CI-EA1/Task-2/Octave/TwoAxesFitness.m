function [fitness] = TwoAxesFitness(y)
  y_1 = 0;
  y_2 = 0;
  N = size(y, 2);
  
  for i=2:N/2
    y_1 = y_1 + (y(i)^2);
  endfor
  
  for i=(N/2)+1:N
    y_2 = y_2 + (y(i)^2);
  endfor
  
  fitness = 1e6*y_1+y_2;
endfunction