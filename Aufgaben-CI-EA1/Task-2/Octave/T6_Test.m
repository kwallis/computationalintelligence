% Test of CSA_ES, CMA_ES and ES_MyMy_Lambda_Sigma
% Authors: Marco Sohm, Kevin Wallis

% Init arguments
graphics_toolkit gnuplot
more off;
randn("state", 7);
 
doSigma = 0;
doSigmaNorm = 0;   % with sigmaP = 0.001;

N = 30;
yP = ones(1, N);
sigmaP = 1;
if(doSigmaNorm)
   sigmaP = 0.001;
endif
my=3;
lambda = 10;
sigma_stop = 10^(-5);

% par.ridge and sharpride dont work
funs = ['CigarFitness'; 'TabletFitness'; 'Ellipsoid2Fitness'; 'ParabolicRidgeFitness'; 'SharpRidgeFitness'; 'DifferentPowersFitness'];
% https://www.gnu.org/software/octave/doc/interpreter/Cell-Arrays-of-Strings.html
funs = ['CigarFitness'; 'TabletFitness'; 'Ellipsoid2Fitness'; 'DifferentPowersFitness'];

colors = ["k"; "r"; "b"; "m"; "c"; "g"];
for i=1: size(funs, 1)
  figure(i)
  clf reset;
  hold on;

  fun = funs(i, :); 
  fun = deblank(fun);
  disp('fun')
  disp(fun)
                                                         %CMA_ES(y_p, sigma, sigma_stop, lambda, my, fun)
  [g_counter, y_opt, m_evolution, f_evolution, mNorm_evolution, f_counter] = CMA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

  disp('generations CMA_ES')
  disp(g_counter)
  disp('func eval of CMA_ES')
  disp(f_counter)
  
  if(doSigma)
    semilogy(m_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
  elseif(doSigmaNorm)
    semilogy(mNorm_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
  else
    semilogy(f_evolution, '-', 'Color',  colors(3), 'linewidth', 2);
  endif
  
  % comparison with CSA_ES
  %[generations, y_opt, sigma_evolution, F_evolution, sigmaNorm_evolution, funcalls] = CSA_ES(yP, sigma, sigma_stop, lambda, my, fun)
  [gen_counter, y_opt, sigma_evol, F_evol, sigmaNorm_evolution, funcalls] = CSA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

  disp('generations CSA_ES')
  disp(gen_counter)
  disp('func eval CSA_ES')
  disp(funcalls)
  
  if(doSigma)
    semilogy(sigma_evol, '-', 'Color',  colors(1), 'linewidth', 2);
  elseif(doSigmaNorm)
    semilogy(sigmaNorm_evolution, '-', 'Color',  colors(1), 'linewidth', 2);
  else
    semilogy(F_evol, '-', 'Color',  colors(1), 'linewidth', 2);
  endif

  % comparison with ES_MyMy_Lambda_Sigma
  rho = 3;
  tau = 1/sqrt(2*N);

  [g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter] = ES_MyMy_Lambda_Sigma(fun, yP, sigmaP, sigma_stop, N, my, rho, lambda, tau);

  disp('generations ES MyMy')
  disp(g_counter)
  disp('func eval of my/my, lambda')
  disp(f_counter)
  
  if(doSigma)
    semilogy(m_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
  elseif(doSigmaNorm)
    semilogy(mNorm_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
  else
    semilogy(f_evolution, '-', 'Color',  colors(2), 'linewidth', 2);
  endif

  xlabel('Generations', 'FontSize', 12);
  if(doSigma)
    ylabel('Mutation', 'FontSize', 12);
  elseif(doSigmaNorm)
    ylabel('Normalized mutation', 'FontSize', 12);
  else
    ylabel('Fitness', 'FontSize', 12);
  endif
  title(['CMA-ES, CSA-ES and \sigmaSA-ES comparision; N=30; fun=' fun], 'FontSize', 12);
  legend('(3/3_I, 10)-CMA-ES', '(3/3_I, 10)-CSA-ES', '(3/3_I, 10)-\sigmaSA-ES'); 

  hold off;
  
  if(doSigma)
    tmp = strcat('./figures/Task6_Sigma_', fun, '.png');
  elseif(doSigmaNorm)
    tmp = strcat('./figures/Task6_SigmaNormalized', fun, '.png');
  else
    tmp = strcat('./figures/Task6_Fitness', fun, '.png');
  endif
  print(tmp, '-dpng');
  disp('-------------------------------------------------')
endfor
