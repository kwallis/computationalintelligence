function [generations, y_opt, sigma_evolution, F_evolution, sigmaNorm_evolution, funcalls] = CMA_ES_RankMy(y_p, sigma, sigma_stop, lambda, my, fun)
  %initialize
  funcalls = 0;
  g_max = 10000;
  N = size(y_p, 2); % search dimension (y_p dimension)
  generations = 0;
  sigma_evolution = [sigma];
  F_evolution = [feval(fun, y_p)];
  sigmaNorm_evolution = [sigma/norm(y_p) * N];
  M = [];
  
  % path vector
  s = ones(1, N);
  
  % constants
  
  tau_c = 1 / (2 / (my * (N + sqrt(2))^2) + (1 - 1 / my) * min(1, (2 * my - 1) / ((N + 2)^2 + my))) ;
  tau_s = 1 + N / 4;
  tau_v = 1 + N / 4;
  D = 1 + tau_s;
  C = eye(N, N); 
  v = ones(1, N); 
  
  do 
    individual.n_tilde = [];
    individual.y_tilde = [];
    individual.F_tilde = [];
    individual.w_tilde = [];  
    
    M = chol(C);
    
    for l=1:lambda 
      % create random vector
      n_l_tilde = randn(1, size(y_p, 2));      
      
      % linear transormation from n with M
      w_l_tilde = (M * n_l_tilde')'; 
      
      % create offspring depending on the random vector
      y_l_tilde = y_p + sigma * w_l_tilde;
      
      % save individual
      individual.n_tilde = [individual.n_tilde; n_l_tilde];
      individual.y_tilde = [individual.y_tilde; y_l_tilde];
      individual.F_tilde = [individual.F_tilde; feval(fun, y_l_tilde)];
      individual.w_tilde = [individual.w_tilde; w_l_tilde];
    endfor
    
    funcalls = funcalls + lambda;
    w_my_recombine = zeros(1, size(y_p, 2)); 
    n_my_recombine = zeros(1, size(y_p, 2));
    rank_my_matrix = zeros(N, N);
    
    % select the my best offsprings of lambda offsprings 
    for i=1:(my) 
      indexBestValue = find(individual.F_tilde == min(individual.F_tilde));
      w_my_recombine = w_my_recombine + individual.w_tilde(indexBestValue, :);
      n_my_recombine = n_my_recombine + individual.n_tilde(indexBestValue, :); 
      rank_my_matrix = rank_my_matrix + individual.w_tilde(indexBestValue, :)' * individual.w_tilde(indexBestValue, :);  
      individual.F_tilde(indexBestValue) = inf;    
      % set the current best F_tilde value to inf get the second best F_tilde value in the next iteration
      % F_tilde can be changed because its not needed anymore
    endfor
    
    w_my_recombine = 1/my * w_my_recombine;
    n_my_recombine = 1/my * n_my_recombine;
    rank_my_matrix = 1/my * rank_my_matrix;             
    y_p = y_p + sigma * w_my_recombine;                              
    
    v = (1 - 1 / tau_v) * v + (my * 1 / tau_v * (2 - 1 / tau_v))^(1/2) * w_my_recombine; 
    % v is a row vector - use (v' * v) instead of (v * v')
	  C = (1 - 1 / tau_c) * C + 1 / tau_c * ( 1 / my * (v' * v) + (1 - 1 / my) * rank_my_matrix) ;
	
	  s = (1 - 1 / tau_s) * s + (my * 1 / tau_s * (2 - 1 / tau_s))^(1/2) * n_my_recombine;     
    
    % slide 15 D.V. Arnold
    sigma = sigma * e^( (norm(s)^2 - N) / (2 * D * N) );              
    
    % mutation over generations
    sigma_evolution = [sigma_evolution ; sigma];
    % fitness over generations
    F_evolution =  [F_evolution ; feval(fun, y_p)];
    % normalized mutation
    sigmaNorm_evolution =  [sigmaNorm_evolution; sigma/norm(y_p) * N];
    
    generations++;                                                   
  until (sigma < sigma_stop || generations > g_max)
  
  y_opt = y_p;
endfunction