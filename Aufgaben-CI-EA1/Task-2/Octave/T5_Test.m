% Test of CSA_ES and CMA_ES
% Authors: Marco Sohm, Kevin Wallis

% Init arguments
graphics_toolkit gnuplot
more off;
randn("state", 7);
 
figure(1)
clf reset;
hold on;

doSigma = 0;
doSigmaNorm = 0;   % with sigmaP = 0.001;

fun = 'QuadraticSphereFitness';
N = 40;
yP = ones(1, N);
sigmaP = 1;
if(doSigmaNorm)
   sigmaP = 0.001;
endif
my=3;
lambda = 10;
sigma_stop = 10^(-5);

colors = ["k"; "r"; "b"; "m"; "c"; "g"];
%[generations, y_opt, sigma_evolution, F_evolution, sigmaNorm_evolution, funcalls] = CSA_ES(yP, sigma, sigma_stop, N, lambda, my, fun)
[gen_counter, y_opt, sigma_evol, F_evol, sigmaNorm_evolution, funcalls] = CSA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

disp('generations CSA_ES')
disp(gen_counter)
disp('func eval CSA_ES')
disp(funcalls)
if(doSigma)
  semilogy(sigma_evol, '-', 'Color',  colors(1), 'linewidth', 2);
elseif(doSigmaNorm)
  semilogy(sigmaNorm_evolution, '-', 'Color',  colors(1), 'linewidth', 2);
else
  semilogy(F_evol, '-', 'Color',  colors(1), 'linewidth', 2);
endif

% comparison with CMA_ES
[gen_counter, y_opt, sigma_evol, F_evol, sigmaNorm_evolution, funcalls] = CMA_ES(yP, sigmaP, sigma_stop, lambda, my, fun);

disp('generations CMA_ES')
disp(gen_counter)
disp('func eval of CMA_ES')
disp(funcalls)
if(doSigma)
  semilogy(sigma_evol, '-', 'Color',  colors(5), 'linewidth', 2);
elseif(doSigmaNorm)
  semilogy(sigmaNorm_evolution, '-', 'Color',  colors(5), 'linewidth', 2);
else
  semilogy(F_evol, '-', 'Color',  colors(5), 'linewidth', 2);
endif
xlabel( 'Generations', 'FontSize', 12);
if(doSigma)
  ylabel( 'Mutation', 'FontSize', 12 );
elseif(doSigmaNorm)
  ylabel( 'Normalized mutation', 'FontSize', 12 );
else
  ylabel( 'Fitness', 'FontSize', 12 );
endif
title('(3/3_I, 10)-CSA-ES and (3/3_I, 10)-CMA-ES comparision; N=40', 'FontSize', 12);
legend('(3/3_I, 10)-CSA-ES', '(3/3_I, 10)-CMA-ES'); 

hold off;

if(doSigma)
  print -color -dpng './figures/Task5Sigma.png'
elseif(doSigmaNorm)
  print -color -dpng './figures/Task5SigmaNormalized.png'
else
  print -color -dpng './figures/Task5.png'
endif