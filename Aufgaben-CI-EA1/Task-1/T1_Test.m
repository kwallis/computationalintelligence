% Test of ES_MyMy_Lambda_Sigma and OneKommaTwo_Meta_ES
% Authors: Marco Sohm, Kevin Wallis

% Init arguments
graphics_toolkit gnuplot
more off;
randn("state", 7);
 
figure(1)
clf reset;
hold on;

doSigma = 1;
doSigmaNorm = 0;

fun = 'QuadraticSphereFitness';
N = 40;
yP = ones(1,N);
sigmaP = 1;
alpha = 1.2;
my=3;
lambda = 10;
gammaValues = [1; 2; 5; 10];  
sigma_stop = 10^(-5);

colors = ["k"; "r"; "b"; "m"; "c"; "g"];

for i=1: size(gammaValues, 1)
  gamma = gammaValues(i,1);
  [gen_counter, y_opt, sigma_evol, F_evol, sigmaNorm_evolution, funcalls] = OneKommaTwo_Meta_ES(fun, yP, sigmaP, alpha, my, lambda, gamma, N, sigma_stop);
  disp('gamma = ')
  disp(gamma)
  disp('generations OneKommaTwo_Meta_ES')
  disp(gen_counter)
  disp('func eval OneKommaTwo_Meta_ES')
  disp(funcalls)
  if(doSigma)
    semilogy(sigma_evol, '-', 'Color',  colors(i), 'linewidth', 2);
  elseif(doSigmaNorm)
    semilogy(sigmaNorm_evolution, '-', 'Color',  colors(i), 'linewidth', 2);
  else
    semilogy(F_evol, '-', 'Color',  colors(i), 'linewidth', 2);
  endif
endfor

% comparison with ES_MyMy_Lambda_Sigma
rho=3;
lambda = 10;
tau = 1/sqrt(2*N);

[g_counter, y_optimal, m_evolution, f_evolution, mNorm_evolution, f_counter] = ES_MyMy_Lambda_Sigma(fun, yP, sigmaP, sigma_stop, N, my, rho, lambda, tau);

disp('generations ES MyMy')
disp(g_counter)
disp('func eval of my/my, lambda')
disp(f_counter)
if(doSigma)
  semilogy(m_evolution, '-', 'Color',  colors(5), 'linewidth', 2);
elseif(doSigmaNorm)
  semilogy(mNorm_evolution, '-', 'Color',  colors(5), 'linewidth', 2);
else
  semilogy(f_evolution, '-', 'Color',  colors(5), 'linewidth', 2);
endif
xlabel( 'Generations', 'FontSize', 12);
if(doSigma)
  ylabel( 'Mutation', 'FontSize', 12 );
elseif(doSigmaNorm)
  ylabel( 'Normalized mutation', 'FontSize', 12 );
else
  ylabel( 'Fitness', 'FontSize', 12 );
endif
title( '[1, 2]-Meta-ES and (3/3_I, 10)-\sigmaSA-ES comparision; N=40', 'FontSize', 12);
legend( ['[1, 2]-Meta-ES \gamma=' int2str(gammaValues(1))], ['[1, 2]-Meta-ES \gamma=' int2str(gammaValues(2))],
 ['[1, 2]-Meta-ES \gamma=' int2str(gammaValues(3))], ['[1, 2]-Meta-ES \gamma=' int2str(gammaValues(4))], '(3/3_I, 10)-\sigmaSA-ES'); 

hold off;

if(doSigma)
  print -color -dpng './figures/Task1Sigma_1.png'
elseif(doSigmaNorm)
  print -color -dpng './figures/Task1SigmaNormalized_1.png'
else
  print -color -dpng './figures/Task1_1.png'
endif