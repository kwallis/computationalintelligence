% function OneKommaTwo_Meta_ES
% Authors: Marco Sohm, Kevin Wallis

% input parameters:
% fun:          function name             [string]
% yP:           initial parent vector     [1 x n] 
% sigmaP:       mutation strength         [real value]
% alpha:        alpha value               [real value]
% my:           my value                  [integer value]
% lambda:       lambda value              [integer value]
% gamma:        gamma value               [integer value]
% N:            search dimension          [integer value]
% sigma_stop:   mutation termination      [real value]

% output parameters:
% g_counter:        number of generations     [integer value]
% y_optimal:        optimal vector y          [1 x n]                               
% sigma_evolution:  mutation over generations [n x 1]     
% F_evolution:      fitness values            [n x 1]
function [g_counter, y_optimal, sigma_evolution, F_evolution, sigmaNorm_evolution, funcalls] = OneKommaTwo_Meta_ES(fun, yP, sigmaP, alpha, my, lambda, gamma, N, sigma_stop)
  
  % initialize            % 1
  funcalls = 0;
  sigma_evolution = [sigmaP];
  F_evolution = [feval(fun, yP)];
  sigmaNorm_evolution = [sigmaP/norm(yP) * N];
  
  F_arr = [];
  sigma_arr = [];
  y_arr = [];
  
  g_counter = 0;          % 2
  g_max = 10000;
  
  do                                % 3
    sigma1_tilde = sigmaP * alpha;  % 4
    sigma2_tilde = sigmaP / alpha;  % 5
    [F1_tilde, y1_tilde, funcalls1] = ES(fun, my, lambda, gamma, sigma1_tilde, yP);   % 6
    [F2_tilde, y2_tilde, funcalls2] = ES(fun, my, lambda, gamma, sigma2_tilde, yP);   % 7
    funcalls = funcalls + funcalls1 + funcalls2;
    
    F_arr = [F1_tilde; F2_tilde];               % put fitness, sigma and y into arrays for sorting and later access
    sigma_arr = [sigma1_tilde; sigma2_tilde];
    y_arr = [y1_tilde; y2_tilde];
    
    indexBestValue = find(F_arr == min(F_arr));
    
    sigmaP = sigma_arr(indexBestValue);         % 8
    yP = y_arr(indexBestValue, :);                 % 9
    g_counter++;                                % 10
    
    % mutation over generations
    sigma_evolution = [sigma_evolution ; sigmaP];
    % fitness over generations
    F_evolution =  [F_evolution ; F_arr(indexBestValue)];
    % normalized mutation
    sigmaNorm_evolution =  [sigmaNorm_evolution; sigmaP/norm(yP) * N];
  until (sigmaP < sigma_stop || g_counter > g_max)  % 11
  
  y_optimal = yP;
  
endfunction