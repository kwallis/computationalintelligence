% function ES slide 10
% Authors: Marco Sohm, Kevin Wallis

% input parameters:
% fun:          function name             [string]
% my:           my value                  [integer value]
% lambda:       lambda value              [integer value]
% gamma:        gamma value               [integer value]
% sigma:        mutation strength         [real value]
% y:            initial parent vector     [1 x n] 

% output parameters:
% F_y:          fitness values            [n x 1]
% y:            vector y                  [1 x n]                               
function [F_y, y, funcalls] = ES(fun, my, lambda, gamma, sigma, y)
  
  y_tilde = [];
  F_tilde = [];
  funcalls = 0;
  
  for i=1:gamma         % 1
    y_tilde = [];
    F_tilde = [];
    for l=1:lambda      % 2
      y_tilde = [y_tilde ; RealOffspring(y, sigma)];    % 3
      F_tilde = [F_tilde ; feval(fun, y_tilde(l, :))];  % 4 (tilde missing on slides?!)
      funcalls++;
    endfor              % 5 
    y = zeros(1, size(y,2));
    % select the my best offsprings of lambda offsprings 
    %indexBestValue = find(F_tilde == min(F_tilde));
    %F_tilde(indexBestValue) = inf; 
    %y = y_tilde(indexBestValue, :); % set y the first time
       
    for i=1:(my)  % (my-1) because of y is initialized with y_tilde(indexBestValue, :)
      indexBestValue = find(F_tilde == min(F_tilde));
      y = y + y_tilde(indexBestValue, :);     % 6 sum 
      F_tilde(indexBestValue) = inf;    
      % set the current best f_lambda value to inf get the second best f_lambda value in the next iteration
      % f_lambda can be changed because its not needed anymore
    endfor
    y = 1/my * y;      % 6  1/my * sum
  endfor               % 7
  F_y = feval(fun, y); % 8
  funcalls++;
endfunction